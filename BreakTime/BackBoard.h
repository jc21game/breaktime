#pragma once
#include "Engine/IGameObject.h"

//テーブルを管理するクラス
class BackBoard : public IGameObject
{
private:
	int hModel_;			//モデル番号

public:
	//コンストラクタ
	BackBoard(IGameObject* parent);

	//デストラクタ
	~BackBoard();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};