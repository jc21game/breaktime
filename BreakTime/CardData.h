#pragma once
#define CARD_NUMMAX 13

enum SUIT
{
	SUIT_SPADE,
	SUIT_HEART,
	SUIT_DIAMOND,
	SUIT_CLUB,
	SUIT_MAX
};

struct CardData
{
	SUIT suit;
	//もっと小さい型でいいかも…
	int number;

};