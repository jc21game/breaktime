#pragma once

#include <time.h>
#include "Engine/IGameObject.h"


//ゲームリザルトを管理するクラス
class TotalResult : public IGameObject
{
	enum ISVICTORY
	{
		ISVICTORY_WIN,
		ISVICTORY_LOSE,
		ISVICTORY_MAX
	};

	enum VICTORY_ACTION
	{
		VICTORY_ACTION_INIT,
		VICTORY_ACTION_FALL,
		VICTORY_ACTION_BOUNCE,
		VICTORY_ACTION_LAND
	}vicAction_;

	enum DEFEAT_ACTION
	{
		DEFEAT_ACTION_INIT,
		DEFEAT_ACTION_FALL,
		DEFEAT_ACTION_LAND,
		DEFEAT_ACTION_LEAN
	}defAction_;

	int hModel_[2];
	ISVICTORY result_;
	float speed_;
	bool isTotal_;
	bool isRound_;

	time_t start_;		// ラウンドリザルト停止開始
	time_t now_;		// ラウンドリザルト停止経過時間
	const double stop_;	// ラウンドリザルト停止する時間

public:
	//コンストラクタ
	TotalResult(IGameObject* parent);

	//デストラクタ
	~TotalResult();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//引数false:負けた true:勝った
	void SetVictory(bool isVictory);

	//もう一度プレイするときの初期化
	void Reset();

	//引数false:負けた true:勝った
	void RoundResult(bool isVictory);

	//ゲーム勝利の動き
	void VictoryMove();

	//ゲーム敗北の動き
	void DefeatMove();

	float GetPositionX() { return position_.x; };
};