#include "Teamlogo.h"
#include "Engine/Image.h"
#include "SplashScene.h"
#include "TeamRing.h"

//コンストラクタ
Teamlogo::Teamlogo(IGameObject * parent)
	:IGameObject(parent, "Teamlogo"), 
	clear_(0), stride_(0.01f), logoState_(LOGO_STATE_FADE)
{
}

//デストラクタ
Teamlogo::~Teamlogo()
{
}

//初期化
void Teamlogo::Initialize()
{
	hPict_ = Image::Load("data/TeamLogo_name.png");
	assert(hPict_ >= 0);
	CreateGameObject<TeamRing>(this);
}

//更新
void Teamlogo::Update()
{
	if (1 < clear_)
	{
		logoState_ = LOGO_STATE_END;
		if (2 < clear_)
		{
			stride_ *= (-1);
		}
	}
	clear_ += stride_;
	if (clear_ < 0)
	{
		KillMe();
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		switch (logoState_)
		{
		case LOGO_STATE_FADE:
			clear_ = 1.0f;	
			break;
		case LOGO_STATE_END:
			((SplashScene*)pParent_)->MoveNext(true);
			KillMe();
			break;
		default :
			break;
		}
	}
	

	Image::SetColor(hPict_, clear_);
}

//描画
void Teamlogo::Draw()
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, (float)POSITION_X, (float)POSITION_Y, (float)0);
	Image::SetMatrix(hPict_, mat);
	Image::Draw(hPict_);
}

//開放
void Teamlogo::Release()
{
}