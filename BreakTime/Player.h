#pragma once
#define WIN_COUNT_PICT 4
#include "Engine/IGameObject.h"

class Hand;
class BetBoard;

//プレイヤを管理するクラス
class Player : public IGameObject
{
protected:
	Hand* pHand_;				//手札
	bool* isSelect_;			//手札選択位置
	int wins_;					//勝数
	int hPict_[WIN_COUNT_PICT];	//画像番号
	bool isCalled_;				//勝負するかどうか（真：勝負　偽：降りる）
	BetBoard* pBetBoard_;		//ポイント表示盤

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	virtual void Draw() override;

	//開放
	void Release() override;

	//初期位置を設定する。PCとNPCで位置が違うため、overrideで解決
	virtual void InitHandCardsPos() = 0;

	//ポイント表示位置を設定
	virtual void InitBetBoardPos() = 0;

	//山札からカードを引き、手札に加える 
	//引数 addPosition:手札の何番目に加えるか 
	virtual void PullCard(int addPosition) = 0;

	//手札からカードを破棄し、捨て札に加える
	//引数 removePosition:手札の何番目に加えるか 
	void DumpCard(int removePosition);

	//最初の五枚ドロー
	void InitPullCard();

	//isSelect_でtrueの要素番目のカード全てに、山札から引いたデータを移す
	void PullSelectCard();

	//最後の五枚削除
	void FinishDumpCard();

	//isSelect_でtrueの要素番目のカードを全て削除
	void DumpSelectCard();

	//手札のselectIndex枚目の選択、非選択を切り替える
	//引数 selectIndex:何枚目の手札か, isSelect:選択するか
	void ChangeSelectState(int selectIndex, bool isSelect);

	//手札のselectIndex枚目のカーソルを表示する
	//引数 selectIndex:何枚目の手札か
	void DisplayCursor(int selectIndex);

	//手札のカーソルを非表示にする
	void HideCursor();

	//勝ち数を増やす
	void WinPlayer();

	//勝負するか決める
	//戻り値：選択が終了したか
	virtual bool SelectFoldOrCall() = 0;

	//選択されているカードがあるか
	//戻り値 真:全て選択されていない, 偽:ひとつでも選択されている
	bool IsAllNotSelect();

	//勝ち数のリセット
	void ResetWins();

	//ドロー処理終了
	bool IsEndPull();

	//ポイント操作
	//戻り値 真：処理終了, 偽：処理中
	virtual bool betPoint() = 0;

	//ポイント初期化
	void ResetPoint();

	//Getter
	//引数 selectIndex:何枚目の手札か
	bool GetIsSelect(int selectIndex) { return isSelect_[selectIndex]; }
	int GetWins() { return wins_; }
	Hand* GetHand() { return pHand_; }
	bool GetIsCalled() { return isCalled_; }
	BetBoard* GetBetBoard() { return pBetBoard_; }

	//Setter
	bool SetIsCalled(bool isFolded) { isCalled_ = isFolded; }

};