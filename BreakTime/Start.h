#pragma once
#include "Engine/IGameObject.h"

#define MOVE_WIDTH 300			//位置xの値
#define MOVE_HEIGHT 600			//位置yの値

//タイトル画像を管理するクラス
class Start : public IGameObject
{
private:
	int hPict_;

	enum STATE
	{
		STATE_DARK,
		STATE_BRIGTH,
		STATE_MAX
	}state_;

	float clear_;		// α値更新
	float stride_;		// ↑の変化式

public:
	//コンストラクタ
	Start(IGameObject* parent);

	//デストラクタ
	~Start();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//アクセス関数
	float Getclear() { return clear_; }
};