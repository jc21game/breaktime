#include "FoldOrCall.h"
#include "Engine/Image.h"

const D3DXVECTOR3 BOARD_POS = D3DXVECTOR3(350, 160, 0);
const D3DXVECTOR3 FOLD_POS = BOARD_POS + D3DXVECTOR3(0, 0, 0);
const D3DXVECTOR3 CALL_POS = BOARD_POS + D3DXVECTOR3(0, 0, 0);


//コンストラクタ
FoldOrCall::FoldOrCall(IGameObject * parent)
	:IGameObject(parent, "FoldOrCall"), hPictBoard_(-1), hPictFold_(-1), hPictCall_(-1), isDisplay_(false),
	select_(SELECT_CALL), drawCount_(0)
{
}

//デストラクタ
FoldOrCall::~FoldOrCall()
{
}

//初期化
void FoldOrCall::Initialize()
{
	//背面画像データのロード
	hPictBoard_ = Image::Load("data/Battle_or_Fold.png");
	assert(hPictBoard_ >= 0);
	Image::SetColor(hPictBoard_, 1.0f);

	//「降りる」画像データのロード
	hPictFold_ = Image::Load("data/Fold.png");
	assert(hPictFold_ >= 0);
	Image::SetColor(hPictFold_, 1.0f);

	//「勝負」画像データのロード
	hPictCall_ = Image::Load("data/Battle.png");
	assert(hPictCall_ >= 0);
	Image::SetColor(hPictCall_, 1.0f);
}

//更新
void FoldOrCall::Update()
{
}

//描画
void FoldOrCall::Draw()
{

	//描画するかどうか
	if (isDisplay_)
	{
		float alpha = abs(sin(D3DXToRadian(drawCount_))) * 0.05f + 1.0f;
		drawCount_+= 2;
		D3DXMATRIX matBoard, matCall, matFold, mat;
		D3DXMatrixScaling(&mat, alpha, alpha, 1.0f);
		D3DXMatrixTranslation(&matFold, FOLD_POS.x, FOLD_POS.y, FOLD_POS.z);
		D3DXMatrixTranslation(&matCall, CALL_POS.x, CALL_POS.y, CALL_POS.z);
		//選択位置を少し点滅
		switch (select_)
		{
			//「勝負」選択時
		case SELECT_CALL:
			Image::SetMatrix(hPictFold_, matFold);
			Image::SetMatrix(hPictCall_, mat * matCall);
			break;
			//「降りる」選択時
		case SELECT_FOLD:
			Image::SetMatrix(hPictFold_, mat * matFold);
			Image::SetMatrix(hPictCall_, matCall);
			break;
		}

		//ボード
		D3DXMatrixTranslation(&matBoard, BOARD_POS.x, BOARD_POS.y, BOARD_POS.z);
		Image::SetMatrix(hPictBoard_, matBoard);
		Image::Draw(hPictBoard_);

		//「降りる」
		Image::Draw(hPictFold_);

		//「勝負」
		Image::Draw(hPictCall_);
	}
	else
	{
		//初期化
		drawCount_ = 0;
		Image::SetColor(hPictCall_, 1.0f);
		Image::SetColor(hPictFold_, 1.0f);
	}

}

//開放
void FoldOrCall::Release()
{
}

void FoldOrCall::Display(bool isDisplay)
{
	isDisplay_ = isDisplay;
}

void FoldOrCall::SelectCursor(int selectPos)
{
	select_ = selectPos;
}
