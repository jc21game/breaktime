#include "Cursor.h"
#include "Card.h"
#include "Engine/Model.h"
#define DISPLAY_CNT_MAX 40
#define VANISH_CNT_MAX (DISPLAY_CNT_MAX + 10)

//コンストラクタ
Cursor::Cursor(IGameObject * parent)
	:IGameObject(parent, "Cursor"), hModel_(-1), display_(false), displayChangeCnt_(0)
{
}

//デストラクタ
Cursor::~Cursor()
{
}

//初期化
void Cursor::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/frame.fbx");
	assert(hModel_ >= 0);
}

//更新
void Cursor::Update()
{
	//選択されているものだけ表示
	display_ = ((Card*)pParent_)->GetSelected();

	displayChangeCnt_++;
	if (displayChangeCnt_ > VANISH_CNT_MAX)
	{
		displayChangeCnt_ = 0;
	}
}

//描画
void Cursor::Draw()
{
	if (display_ && displayChangeCnt_ < DISPLAY_CNT_MAX)
	{
		Model::SetMatrix(hModel_, worldMatrix_);
		Model::Draw(hModel_);
	}
}

//開放
void Cursor::Release()
{
}