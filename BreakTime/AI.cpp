#include "AI.h"
#include "Hand.h"
#include "NonPlayerCharacter.h"
#include "BetBoard.h"

AI::AI(Player* parent, int changeCostMax) : emotion_(EMOTION_STANDARD), changeCostMax_(changeCostMax), targetHand_(POKER_HAND_ROYAL_FLUSH), parent_(parent)
{
}

AI::~AI()
{
}

void AI::SelectTargetHand()
{
	//現在の手から、changeCostMax_以内で作れる手を探す
	//POKER_HAND_ROYAL_FLUSHから順に狙う手を落としていく
	//フラッシュ、ストレートが可能か
	bool canFlush = CanFlush();
	bool canStraight = CanStraight();

	//両方できるならストレートフラッシュを狙って終了
	/*if (canFlush && canStraight)
	{
		targetHand_ = POKER_HAND_STRAIGHT_FLUSH;
		return;
	}*/

	//できないなら
	//現在の手から狙う手を決める配列
	POKER_HAND handToTarget[2][POKER_HAND_MAX] = {
		{POKER_HAND_ONE, POKER_HAND_THREE, POKER_HAND_FULL_HOUSE, POKER_HAND_FOUR, POKER_HAND_STRAIGHT, POKER_HAND_FLUSH, POKER_HAND_FULL_HOUSE, POKER_HAND_FOUR, POKER_HAND_STRAIGHT_FLUSH, POKER_HAND_ROYAL_FLUSH},
		{POKER_HAND_THREE, POKER_HAND_FOUR, POKER_HAND_FOUR, POKER_HAND_FOUR, POKER_HAND_STRAIGHT, POKER_HAND_FLUSH, POKER_HAND_FULL_HOUSE, POKER_HAND_FOUR, POKER_HAND_STRAIGHT_FLUSH, POKER_HAND_ROYAL_FLUSH},
	};
	POKER_HAND nowHand = parent_->GetHand()->GetPokerHand();
	if (changeCostMax_ == 1 || changeCostMax_ == 2)
	{
		targetHand_ = handToTarget[changeCostMax_ - 1][nowHand];
	}
	else if (changeCostMax_ == 0)
	{
		targetHand_ = nowHand;
	}

	//フルハウスより弱い手なら
	if (targetHand_ < POKER_HAND_FULL_HOUSE)
	{
		if (canFlush)
		{
			targetHand_ = POKER_HAND_FLUSH;
		}
		else if (canStraight)
		{
			targetHand_ = POKER_HAND_STRAIGHT;
		}
	}
}

bool AI::CanFlush()
{
	//一番多い柄の枚数
	int suitCnt = parent_->GetHand()->GetManySuitCnt();
	//柄は目標に足りた枚数あるのか
	if (suitCnt < HAND_MAX - changeCostMax_)
	{
		//ないなら偽を返す
		return false;
	}
	else
	{
		//足りるなら真を返す
		return true;
	}
}

bool AI::CanStraight()
{
	//手札の数値データを受け取る
	std::vector<int> data = parent_->GetHand()->GetNumVec();
	//数が大きい順に数値を並び替え
	std::sort(data.begin(), data.end(), [](const int a, int b) {
		return a < b;
	});

	//中空きのストレートの判定ができないアルゴリズム案
	int smallOneNum = data[0];
	//ストレート(1ずつ減っていく)判定の成功/不成功回数
	int straightTrueCnt = 0;
	int straightFalseCnt = 0;
	//許容値以上の差で失敗とするためのバッファー
	int diffBuffer = 0;
	//前のに比べて一つ小さい数値。最大値を初期化で入れる
	for (int i = 1; i < HAND_MAX; i++)
	{
		//前回のループから一つ小さくする
		smallOneNum--;
		//数値が1ずつ減っているか
		if (data[i] == smallOneNum)
		{
			//減っているなら(成功)
			straightTrueCnt++;
		}
		else
		{
			//目標数1ずつ減っているか
			if (straightTrueCnt >= HAND_MAX - changeCostMax_ - 1)
			{
				//ストレート可能
				straightNumMin_ = smallOneNum;
				straightNumMax_ = smallOneNum + straightTrueCnt;
				return true;
			}

			//減っていないなら(不成功)
			/*diffBuffer += smallOneNum - data[i];
			if (diffBuffer > changeCostMax_)
			{*/
				//失敗
				straightFalseCnt++;
				//trueCntを初期化
				straightTrueCnt = 0;
				//現在値を基準に
				smallOneNum = data[i];
				//falseCntが許容値を超えた
				if (straightFalseCnt > changeCostMax_)
				{
					//ストレート不可
					return false;
				}
			//}
		}
	}

	straightNumMin_ = data[HAND_MAX - 1];
	straightNumMax_ = data[0];
	return true;
}

void AI::DecideBetPoint()
{
	//現在の所持ポイントを取得
	int point = ((BetBoard*)(parent_->GetBetBoard()))->GetPoint();

	//狙える役によって賭けるポイントを変更
	int betPoint = 0;
	if (targetHand_ <= POKER_HAND_TWO)
	{
		betPoint = point / 10 / 9 * 10;
		emotion_ = EMOTION_WORRY;
	}
	else if (targetHand_ <= POKER_HAND_FOUR)
	{
		betPoint = point / 10 / 9 * 2 * 10;
		emotion_ = EMOTION_LOOK;
	}
	else if (targetHand_ <= POKER_HAND_STRAIGHT_FLUSH)
	{
		betPoint = point / 10 / 3 * 10;
		emotion_ = EMOTION_LOOK;
	}
	else
	{
		betPoint = point / 10 / 9 * 4 * 10;
		emotion_ = EMOTION_STANDARD;
	}

	//所持ポイントより少ないか
	if (point > betPoint)
	{
		//betPoint分賭ける
		((BetBoard*)(parent_->GetBetBoard()))->AddBetPoint(betPoint);
	}
	else
	{
		//所持ポイントをすべて賭ける
		((BetBoard*)(parent_->GetBetBoard()))->AddBetPoint(point);
	}
}

bool AI::FoldOrCall()
{
	POKER_HAND hand = ((Hand*)parent_->GetHand())->GetPokerHand();
	int betPoint = ((BetBoard*)(parent_->GetBetBoard()))->GetBetPoint();

	//現在の所持ポイントを取得
	int point = ((BetBoard*)(parent_->GetBetBoard()))->GetPoint();

	int random = rand() % 10;

	//賭けていたポイントに応じて判断
	if (betPoint <= point / 10 / 9 * 10)
	{
		//勝負
		emotion_ = EMOTION_STANDARD;
		return true;
	}
	else if (betPoint <= point / 10 / 9 * 2 * 10)
	{
		if (POKER_HAND_ONE > hand)
		{
			emotion_ = EMOTION_SURPRISED;
			//一定確率で勝負
			if (random < 2)
			{
				return true;
			}
			else
			{
				//降りる
				return false;
			}
		}
		else
		{
			emotion_ = EMOTION_STANDARD;
			//勝負
			return true;
		}
	}
	else if(betPoint <= point / 10 / 3 * 10)
	{
		if (POKER_HAND_TWO > hand)
		{
			emotion_ = EMOTION_SURPRISED;
			//一定確率で勝負
			if (random < 5)
			{
				return true;
			}
			else
			{
				//降りる
				return false;
			}
		}
		else
		{
			//勝負
			emotion_ = EMOTION_STANDARD;
			return true;
		}
	}
	else
	{
		if (POKER_HAND_THREE > hand)
		{
			emotion_ = EMOTION_SURPRISED;
			//一定確率で勝負
			if (random < 5)
			{
				return true;
			}
			else
			{
				//降りる
				return false;
			}
		}
		else
		{
			//勝負
			emotion_ = EMOTION_STANDARD;
			return true;
		}
	}

	return true;
}

void AI::SetEmotion(EMOTION emotion)
{
	//存在しないものではないか確認
	if (emotion >= 0 && emotion < EMOTION_MAX)
	{
		emotion_ = emotion;
	}
}
