#include "BetBoard.h"
#include "Engine/Image.h"
#include "BetBoard.h"

#define NUM_HEIGHT			1			//枠上端からの高さ
#define NUM_WIDTH			1			//枠左端からの幅
#define DISPLAY_TIME		180			//テキストの表示時間(フレーム数)
#define NUMBER_HEIGHT		-10			//枠に対する数字の高さ
#define NUMBER_WIDTH		60			//枠に対する数字の幅
#define NUMBER_SPACING		40			//数字の幅
#define POINT_DEFAULT		450			//ポイントの初期値
#define ARROW_UP_POS_X		20 			//ポイント選択時の上矢印のx位置
#define ARROW_UP_POS_Y		-65			//ポイント選択時の上矢印のy位置
#define ARROW_DOWN_POS_X	21			//ポイント選択時の下矢印のx位置
#define ARROW_DOWN_POS_Y	55			//ポイント選択時の下矢印のy位置

//コンストラクタ
BetBoard::BetBoard(IGameObject * parent)
	:IGameObject(parent, "BetBoard"), hPictBet_(-1), hPictPoint_(-1), betPoint_(0), point_(POINT_DEFAULT), isDisplayArrow_(false),
	pEffect_(nullptr)
{
	//画像番号を-1で初期化
	for (int i = 0; i < NUMBER_MAX; i++)
	{
		hPictNumber_[i] = -1;
	}

	for (int i = 0; i < ARROW_MAX; i++)
	{
		hPictArrow_[i] = -1;
	}
}

//デストラクタ
BetBoard::~BetBoard()
{
}

//初期化
void BetBoard::Initialize()
{
	//賭けポイント画像データのロード
	hPictBet_ = Image::Load("data/Betting_Point_S_T.png");
	assert(hPictBet_ >= 0);
	Image::SetColor(hPictBet_, 1.0f);

	//所持ポイント画像データのロード
	hPictPoint_ = Image::Load("data/Possession_Point_S_T.png");
	assert(hPictPoint_ >= 0);
	Image::SetColor(hPictPoint_, 1.0f);

	//テキストのファイルパスを格納（格納しているパスは仮）
	fileName_[NUMBER_0] = "0";
	fileName_[NUMBER_1] = "1";
	fileName_[NUMBER_2] = "2";
	fileName_[NUMBER_3] = "3";
	fileName_[NUMBER_4] = "4";
	fileName_[NUMBER_5] = "5";
	fileName_[NUMBER_6] = "6";
	fileName_[NUMBER_7] = "7";
	fileName_[NUMBER_8] = "8";
	fileName_[NUMBER_9] = "9";


	//数字画像データのロード
	for (int i = 0; i < NUMBER_MAX; i++)
	{
		hPictNumber_[i] = Image::Load("data/number/SS/" + fileName_[i] + "_SS.png");
		assert(hPictNumber_[i] >= 0);
		Image::SetColor(hPictNumber_[i], 1.0f);
	}

	//矢印画像データのロード
	hPictArrow_[ARROW_UP] = Image::Load("data/Choice_Up_S.png");
	Image::SetColor(hPictArrow_[ARROW_UP], 1.0f);
	hPictArrow_[ARROW_DOWN] = Image::Load("data/Choice_Down_S.png");
	Image::SetColor(hPictArrow_[ARROW_DOWN], 1.0f);

	////エフェクト生成
	//LPD3DXBUFFER err = 0;		//エラー用
	//if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice, "ArrowShader.hlsl",
	//	NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	//{
	//	//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
	//	MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
	//}

}

//更新
void BetBoard::Update()
{
}

//描画
void BetBoard::Draw()
{
	//ボード、ポイントを描画
	DrawPoint();
}

//開放
void BetBoard::Release()
{
}

void BetBoard::AddBetPoint(int point)
{
	//所持ポイントと賭けポイントが両方とも負にならない場合
	if ((point_ - point) >= 0 && (betPoint_ + point) >= 0)
	{
		//ポイント移動
		point_ -= point;
		betPoint_ += point;
	}
}

void BetBoard::DrawPoint()
{
	//賭けポイントを描画
	//ボード
	D3DXMATRIX matrix;
	D3DXMatrixTranslation(&matrix, betPosition_.x, betPosition_.y, betPosition_.z);
	Image::SetMatrix(hPictBet_, matrix);
	Image::Draw(hPictBet_);
	//数字の表示位置
	D3DXVECTOR3 numPos = betPosition_ + D3DXVECTOR3(NUMBER_WIDTH, NUMBER_HEIGHT, 0);
	//数字
	//一の位
	DrawNumber(numPos, betPoint_ % 10);
	//十の位
	//ポイントが三桁以上か0ではないとき描画
	if (((betPoint_ / 10) % 10) != 0 || betPoint_ >= 100)
	{
		DrawNumber(numPos - D3DXVECTOR3(NUMBER_SPACING, 0, 0), (betPoint_ / 10) % 10);
	}
	//百の位
	//0ではないとき描画
	if ((betPoint_ / 100) != 0)
	{
		DrawNumber(numPos - D3DXVECTOR3(NUMBER_SPACING * 2, 0, 0), betPoint_ / 100);
	}

	//矢印表示するかどうか
	if (isDisplayArrow_)
	{
		//カウントを進める
		arrowCount_++;
		////pEffectを使って描画
		//pEffect_->Begin(NULL, 0);
		////パス設定
		//pEffect_->BeginPass(0);
		//矢印描画
		DrawArrow();
		////パス終了
		//pEffect_->EndPass();
	}
	else
	{
		arrowCount_ = 0;
	}

	//所持ポイントを描画
	//ボード
	D3DXMatrixTranslation(&matrix, pointPosition_.x, pointPosition_.y, pointPosition_.z);
	Image::SetMatrix(hPictPoint_, matrix);
	Image::Draw(hPictPoint_);
	//数字の表示位置
	numPos = pointPosition_ + D3DXVECTOR3(NUMBER_WIDTH, NUMBER_HEIGHT, 0);
	//数字
	//一の位
	DrawNumber(numPos, point_ % 10);
	//十の位
	//ポイントが三桁以上か0ではないとき描画
	if (((point_ / 10) % 10) != 0 || point_ >= 100)
	{
		DrawNumber(numPos - D3DXVECTOR3(NUMBER_SPACING, 0, 0), (point_ / 10) % 10);
	}
	//百の位
	//0ではないとき描画
	if ((point_ / 100) != 0)
	{
		DrawNumber(numPos - D3DXVECTOR3(NUMBER_SPACING * 2, 0, 0), point_ / 100);
	}
}

void BetBoard::DrawNumber(D3DXVECTOR3 position, int number)
{
	//位置を設定
	D3DXMATRIX matrix;
	D3DXMatrixTranslation(&matrix, position.x, position.y, position.z);
	Image::SetMatrix(hPictNumber_[number], matrix);
	//描画
	Image::Draw(hPictNumber_[number]);
}

void BetBoard::DrawArrow()
{
	D3DXMATRIX matrix;
	//上矢印
	D3DXMatrixTranslation(&matrix, betPosition_.x + ARROW_UP_POS_X, betPosition_.y + ARROW_UP_POS_Y, betPosition_.z);
	Image::SetMatrix(hPictArrow_[ARROW_UP], matrix);
	Image::SetColor(hPictArrow_[ARROW_UP], abs(sin(D3DXToRadian(arrowCount_))) + 0.5f);
	Image::Draw(hPictArrow_[ARROW_UP]);

	//下矢印
	D3DXMatrixTranslation(&matrix, betPosition_.x + ARROW_DOWN_POS_X, betPosition_.y + ARROW_DOWN_POS_Y, betPosition_.z);
	Image::SetMatrix(hPictArrow_[ARROW_DOWN], matrix);
	Image::SetColor(hPictArrow_[ARROW_DOWN], abs(cos(D3DXToRadian(arrowCount_))) + 0.5f);
	Image::Draw(hPictArrow_[ARROW_DOWN]);

}

void BetBoard::DisplayArrow(bool isDisplay)
{
	isDisplayArrow_ = isDisplay;
}

void BetBoard::ResetPoint()
{
	point_ = POINT_DEFAULT;
	betPoint_ = 0;
}
