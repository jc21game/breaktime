#pragma once
#include <string>
#include "CardData.h"
#include "Emotion.h"

//class NonPlayerCharacter;
class Player;
class Hand;
enum POKER_HAND;


class AI
{
protected:
	Player* parent_;
	EMOTION emotion_;
	// 現在の手から何枚変更するかの許容限界値
	int changeCostMax_;
	// 狙う手
	POKER_HAND targetHand_;
	//ストレートが可能な場合の連続した数値の最小・最大
	int straightNumMin_;
	int straightNumMax_;

public:
	//引数ありコンストラクタ
	//引数：pPlayer	親クラス(プレイヤー),	changecostMax 最大交換枚数
	AI(Player* pPlayer, int changeCostMax);
	virtual ~AI();
	// 狙う手を決める
	void SelectTargetHand();
	
	//フラッシュが許容限界内で可能かどうか
	//戻り値 真：可能, 偽：不可能
	bool CanFlush();

	//ストレートが許容限界内で可能かどうか
	//戻り値 真：可能, 偽：不可能
	bool CanStraight();

	//変更するカードを決める
	virtual void SelectChangeCard() = 0;

	//手札の強さをもとに感情を決定
	virtual void PowerToEmotion() = 0;

	//賭けるポイントを決める
	void DecideBetPoint();

	//勝負するか選択する
	//戻り値：勝負するかどうか
	bool FoldOrCall();

	//感情を変化
	//引数：emotion 感情
	void SetEmotion(EMOTION emotion);

	//アクセス関数
	EMOTION GetEmotion() { return emotion_; }
};