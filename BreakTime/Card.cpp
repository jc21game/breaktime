#include <string>
#include <stdlib.h>
#include <time.h>
#include "Card.h"
#include "Cursor.h"
#include "Engine/Model.h"
#include "PlayScene.h"

#define GATHER_FLAME 30		//カード並べ替え時のフレーム数
#define PULL_FLAME 30		//ドロー時のフレーム数
#define DISCARD_FLAME 30	//カードを捨てる時のフレーム数
#define SHUFFLE_FLAME 30	//シャッフル時に山札に戻るまでのフレーム数

//コンストラクタ
Card::Card(IGameObject * parent)
	:IGameObject(parent, "Card"), hModel_(-1), display_(false), selected_(false), isClone_(false), moveFrame_(0), nowFrame_(0), state_(CARD_STATE_STANDARD), nextState_(CARD_STATE_STANDARD), rotateRateY_(0.0f), isInitState_(false)
{
}

//デストラクタ
Card::~Card()
{
}

//初期化
void Card::Initialize()
{
	CreateGameObject<Cursor>(this);

	//元となるモデルをロード
	hModel_ = Model::Load("Data/Cards/Card.fbx", true);
	assert(hModel_ >= 0);
}

//更新
void Card::Update()
{
	switch (state_)
	{
	case CARD_STATE_PULL:
		nowFrame_ = Model::GetAnimFrame(hModel_);
		//PCドローアニメーションの終了
		if (nowFrame_ == 30)
		{
			Model::SetAnimFrame(hModel_, 0, 0, 1.0f);
			rotate_.y = 180;
			move_ = D3DXVECTOR3(0, 0, 0);
		}
		//NPCドローアニメーションの終了
		else if (nowFrame_ == 60)
		{
			Model::SetAnimFrame(hModel_, 0, 0, 1.0f);
			move_ = D3DXVECTOR3(0, 0, 0);
		}
		break;

	case CARD_STATE_DISCARD:
		if (isClone_)
		{
			moveFrame_++;
			if (moveFrame_ >= DISCARD_FLAME)
			{
				move_ = D3DXVECTOR3(0, 0, 0);
				moveFrame_ = 0;
				state_ = nextState_;
			}
			else
			{
				rotate_.x -= 90 / DISCARD_FLAME;
				rotate_.y += ((random_ % 20) - 10) / 10.0f;
			}
		}
		break;

	case CARD_STATE_RESET:
		if (!isInitState_)
		{
			move_ = (D3DXVECTOR3(6, 0, 0) - position_) / SHUFFLE_FLAME;
			rotateRateY_ = -(rotate_.y / SHUFFLE_FLAME);
			rotate_.z = 180;
			rotate_.y = 0;
			isInitState_ = true;
		}

		if (moveFrame_ <= SHUFFLE_FLAME)
		{
			//rotate_.y += rotateRateY_;
			moveFrame_++;
		}
		else
		{
			state_ = CARD_STATE_STANDARD;
			move_ = D3DXVECTOR3(0, 0, 0);
			moveFrame_ = 0;
			KillMe();
		}
	}

	//移動
	position_ += move_;
}

//描画
void Card::Draw()
{
	//表示状態なら表示
	if (display_ && hModel_ > -1)
	{
		Model::SetMatrix(hModel_, worldMatrix_);
		Model::Draw(hModel_);
	}
}

//開放
void Card::Release()
{
}

void Card::LoadModelData()
{
	//テクスチャのパス
	const std::string CARD_PATHHEAD = "Data/Cards/";
	const std::string CARD_PATHTAIL = ".png";
	const std::string SUIT_PATH[SUIT_MAX] = {"Spade/Spade", "Heart/Heart", "Diamond/Diamond", "Club/Club"};
	const std::string NUMBER_PATH[CARD_NUMMAX] = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "1" };
	std::string path = CARD_PATHHEAD + SUIT_PATH[data_.suit] + NUMBER_PATH[data_.number - 1] + CARD_PATHTAIL;
	//モデルデータのロード
	/*hModel_ = Model::Load(path);
	assert(hModel_ >= 0);*/
	//Test
	/*hModel_ = Model::Load("Data/Cards/Spade/Spade1.fbx");
	assert(hModel_ >= 0);*/

	//テクスチャを任意のものに変更
	Model::SetTexture(path, hModel_);
}

Card* Card::Clone(IGameObject* pParent)
{
	Card* pCard = CreateGameObject<Card>(pParent);
	pCard->position_ = position_ - pParent->GetPosition();
	pCard->position_.y -= 0.3f;
	pCard->data_ = data_;
	pCard->display_ = display_;
	pCard->isClone_ = true;

	pCard->LoadModelData();
	pCard->rotate_.y += 180;

	return pCard;
}

//NPCドローアニメーションを再生
void Card::StartNPCPullAnimation(D3DXVECTOR3 startPosition, D3DXVECTOR3 endPosition)
{
	//カードを表示
	display_ = true;

	//カード位置を山札に
	SetPosition(startPosition);

	//山札位置から手札のカード位置までのベクトルを求める
	move_ = (endPosition - position_) / PULL_FLAME;

	//アニメーションをセット
	Model::SetAnimFrame(hModel_, 31, 60, 1.0f);

	state_ = CARD_STATE_PULL;
}

//PCドローアニメーションを再生
void Card::StartPCPullAnimation(D3DXVECTOR3 startPosition, D3DXVECTOR3 endPosition)
{
	//Y角度をもとに戻す
	rotate_.y = 0;

	//カードを表示
	display_ = true;

	//カード位置を山札に
	SetPosition(startPosition);

	//山札位置から手札のカード位置までのベクトルを求める
	move_ = (endPosition - position_) / PULL_FLAME;

	//アニメーションをセット
	Model::SetAnimFrame(hModel_, 1, 30, 1.0f);

	state_ = CARD_STATE_PULL;
}

int Card::GetAnimationFrame()
{
	return Model::GetAnimFrame(hModel_);
}

bool Card::IsEndPullAnimation()
{
	if (Model::GetAnimFrame(hModel_) == 0)
	{
		return true;
	}

	return false;
}

void Card::GatherMoveCards(D3DXVECTOR3 endPosition, int positionNum)
{
	//真ん中のカード
	if (positionNum == MIDDLE_CARD)
	{
		SetPosition(endPosition);
		move_ = D3DXVECTOR3(0, 0, 0);
	}

	//左二枚のカード
	else if (positionNum < MIDDLE_CARD)
	{
		if (endPosition.x < position_.x)
		{
			SetPosition(endPosition);
			move_ = D3DXVECTOR3(0, 0, 0);
		}
		else
		{
			//現在位置から集まる位置までのベクトルを求める
			move_ = (endPosition - removePosition_) / GATHER_FLAME;
		}
	}

	//右二枚のカード
	else if (positionNum > MIDDLE_CARD)
	{
		if (endPosition.x > position_.x)
		{
			SetPosition(endPosition);
			move_ = D3DXVECTOR3(0, 0, 0);
		}
		else
		{
			//現在位置から集まる位置までのベクトルを求める
			move_ = (endPosition - removePosition_) / GATHER_FLAME;
		}
	}
}

void Card::RemoveCards(D3DXVECTOR3 startPosition, int positionNum)
{
	if (positionNum == MIDDLE_CARD)
	{
		SetPosition(startPosition);
		move_ = D3DXVECTOR3(0, 0, 0);
	}

	//左二枚のカード
	else if (positionNum < MIDDLE_CARD)
	{
		if (removePosition_.x >= position_.x)
		{
			SetPosition(removePosition_);
			move_ = D3DXVECTOR3(0, 0, 0);
		}
		else
		{
			move_ = (removePosition_ - startPosition) / GATHER_FLAME;
		}
	}

	//右二枚のカード
	else if (positionNum > MIDDLE_CARD)
	{
		if (removePosition_.x <= position_.x)
		{
			SetPosition(removePosition_);
			move_ = D3DXVECTOR3(0, 0, 0);
		}
		else
		{
			move_ = (removePosition_ - startPosition) / GATHER_FLAME;
		}
	}
}

void Card::Discard(D3DXVECTOR3 endPosition)
{

	//乱数準備
	//乱数の初期化
	srand(time(NULL));
	//乱数
	random_ = rand();

	endPosition.x += ((random_ % 20) - 10) / 50.0f;
	endPosition.z += ((random_ % 20) - 10) / 50.0f;

	move_ = (endPosition - position_) / DISCARD_FLAME;

	state_ = CARD_STATE_DISCARD;
	
}

void Card::moveToDeck()
{
	if (state_ == CARD_STATE_DISCARD)
	{
		nextState_ = CARD_STATE_WAIT;
	}
	else
	{
		state_ = CARD_STATE_WAIT;
	}
}
