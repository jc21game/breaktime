#include "Engine/Image.h"
#include "TeamRing.h"
#include "TeamLogo.h"

//コンストラクタ
TeamRing::TeamRing(IGameObject * parent)
	:IGameObject(parent, "TeamRing"), hPict_(0), alpha_(2), fade_(FADE_IN), 
	spinSpeed_(7.20f), shrinkSpeed_(4.0f), shrink_(1000.0f)
{
}

//デストラクタ
TeamRing::~TeamRing()
{
}

//初期化
void TeamRing::Initialize()
{
	hPict_ = Image::Load("data/TeamLogo_ring.png");
	Image::SetColor(hPict_, 1);
	scale_ = D3DXVECTOR3(shrink_, shrink_, shrink_);
}

//更新
void TeamRing::Update()
{
	alpha_ = ((Teamlogo*)pParent_)->GetAlpha();
	if (1 < alpha_)
	{
		fade_ = FADE_OUT;
	}
	switch (fade_)
	{
	case FADE_IN:
		rotate_.z += 7.20f;	
		shrink_ -= shrinkSpeed_;
		break;
	case FADE_OUT:
		alpha_ -= 0.01f;
		rotate_.z = 0;
		Image::SetColor(hPict_, alpha_);
		break;
	}
	if (alpha_ < 0)
	{
		KillMe();
	}
	scale_ = D3DXVECTOR3(shrink_, shrink_, shrink_);

}

//描画
void TeamRing::Draw()
{
	D3DXMATRIX matrix;
	matrix = RotationPict();
	
	//D3DXVec3TransformCoord(&scale_, &scale_, &matrix);
	Image::SetMatrix(hPict_, matrix);
	Image::Draw(hPict_);
}

//開放
void TeamRing::Release()
{
}

D3DXMATRIX TeamRing::RotationPict()
{
	D3DXMATRIX matrix;
	D3DXMatrixTranslation(&matrix, 0, 0, 0);

	D3DXMATRIX alound, prepMat, reneMat;
	D3DXMatrixRotationZ(&alound, D3DXToRadian(rotate_.z));
	D3DXMatrixTranslation(&prepMat, (float)-380, (float)-250, (float)0);
	prepMat *= alound;
	matrix *= prepMat;

	D3DXMatrixTranslation(&reneMat, (float)380 + POSITION_X, (float)250 + POSITION_Y, (float)0);
	matrix *= reneMat;

	return matrix;
}

