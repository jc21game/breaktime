#include "BackBoard.h"
#include "Engine/Model.h"
#include "Engine/Image.h"

//コンストラクタ
BackBoard::BackBoard(IGameObject * parent)
	:IGameObject(parent, "BackBoard"), hModel_(-1)
{
}

//デストラクタ
BackBoard::~BackBoard()
{
}

//初期化
void BackBoard::Initialize()
{
	//モデルデータのロード
	//hModel_ = Model::Load("data/backGround_Board.fbx");
	//assert(hModel_ >= 0);
	hModel_ = Image::Load("data/back.jpg");
	Image::SetColor(hModel_, 1.0f);
	position_.z = 1;
}

//更新
void BackBoard::Update()
{
	//rotate_.y += 1;
}

//描画
void BackBoard::Draw()
{
	/*Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);*/
	Image::SetMatrix(hModel_, worldMatrix_);
	Image::Draw(hModel_);
}

//開放
void BackBoard::Release()
{
}