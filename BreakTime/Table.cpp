#include "Table.h"
#include "Engine/Model.h"

//コンストラクタ
Table::Table(IGameObject * parent)
	:IGameObject(parent, "Table"), hModel_(-1)
{
}

//デストラクタ
Table::~Table()
{
}

//初期化
void Table::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/table.fbx");
	assert(hModel_ >= 0);
	rotate_.y = 90;
}

//更新
void Table::Update()
{
}

//描画
void Table::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Table::Release()
{
}