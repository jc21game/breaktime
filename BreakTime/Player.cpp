#include "./Engine/Camera.h"
#include "Player.h"
#include "PlayScene.h"
#include "Hand.h"
#include "Deck.h"
#include "Discards.h"
#include "Card.h"
#include "Log.h"
#include "BetBoard.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), pHand_(nullptr), isSelect_(nullptr), wins_(0), isCalled_(false), pBetBoard_(nullptr)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//手札生成
	pHand_ = CreateGameObject<Hand>(this);
	InitHandCardsPos();
	//選択フラグの初期化
	isSelect_ = new bool[HAND_MAX];
	for (int i = 0; i < HAND_MAX; i++)
	{
		isSelect_[i] = false;
	}

	//ポイント表示盤の生成
	pBetBoard_ = CreateGameObject<BetBoard>(this);
	InitBetBoardPos();

	////勝数表示用画像データをロード
	//hPict_[0] = Image::Load("data/Star0.png");
	//assert(hPict_[0] >= 0);
	//Image::SetColor(hPict_[0], 1);
	//hPict_[1] = Image::Load("data/Star1.png");
	//assert(hPict_[1] >= 0);
	//Image::SetColor(hPict_[1], 1);
	//hPict_[2] = Image::Load("data/Star2.png");
	//assert(hPict_[2] >= 0);
	//Image::SetColor(hPict_[2], 1);
	//hPict_[3] = Image::Load("data/Star3.png");
	//assert(hPict_[3] >= 0);
	//Image::SetColor(hPict_[3], 1);

	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -7));
	pCamera->SetTarget(D3DXVECTOR3(0, 2, 0));
}

//更新
void Player::Update()
{
}

//描画
void Player::Draw()
{
}

//開放
void Player::Release()
{
	delete(isSelect_);
}


void Player::DumpCard(int removePosition)
{
	CardData* dump = pHand_->RemoveCard(removePosition);
	((PlayScene*)pParent_)->GetDiscards()->AddCard(dump);
}

void Player::InitPullCard()
{
	for ( int i = 0; i < HAND_MAX; i++ )
	{
		PullCard(i);
	}
}

void Player::PullSelectCard()
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		//選択されているなら
		if (isSelect_[i])
		{
			//そのカードにデータを与える
			PullCard(i);
			ChangeSelectState(i, false);
		}
	}
}

void Player::FinishDumpCard()
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		DumpCard(i);
	}
}

void Player::DumpSelectCard()
{
	int count = 0;			//交換枚数

	for (int i = 0; i < HAND_MAX; i++)
	{
		//選択されているなら
		if (isSelect_[i])
		{
			//そのカードを廃棄
			DumpCard(i);
			count++;
		}
	}

	//交換メッセージ
	if (((PlayScene*)pParent_)->GetPlayerCharacter() == this)
	{
		((PlayScene*)pParent_)->GetLog()->AddTemplate(TEMPLATE_TRADE, TEMPLATE_PC, count, NULL);
	}
	else
	{
		((PlayScene*)pParent_)->GetLog()->AddTemplate(TEMPLATE_TRADE, TEMPLATE_NPC, count, NULL);
	}
}

void Player::ChangeSelectState(int selectIndex, bool isSelect)
{
	//選択されているなら
	if (isSelect)
	{
		isSelect_[selectIndex] = true;
		pHand_->GetChild(selectIndex)->SetPositionY(0.3f);
	}
	//選択されていないなら
	else
	{
		isSelect_[selectIndex] = false;
		pHand_->GetChild(selectIndex)->SetPositionY(0);
	}
}

void Player::DisplayCursor(int selectIndex)
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		if (i == selectIndex)
		{
			((Card*)(pHand_->GetChild(i)))->SetSelected(true);
		}
		else
		{
			((Card*)(pHand_->GetChild(i)))->SetSelected(false);
		}

	}
}

void Player::HideCursor()
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		//カーソルを非表示に
		((Card*)(pHand_->GetChild(i)))->SetSelected(false);
	}
}

void Player::WinPlayer()
{
	wins_++;
}


bool Player::IsAllNotSelect()
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		if (isSelect_[i] == true)
		{
			return false;
		}
	}
	return true;
}

void Player::ResetWins()
{
	wins_ = 0;
}

bool Player::IsEndPull()
{
	

	return false;
}

void Player::ResetPoint()
{
	pBetBoard_->ResetPoint();
}


