#pragma once
#include <vector>
#include "Engine/global.h"
#include "CardData.h"
#include "Engine/Image.h"

//前方宣言
class Deck;
class Discards;
class Player;
class Hand;
class Log;
class TotalResult;
class Help;
class Text;

enum STATE
{
	STATE_SHUFFLE,				//シャッフルしている
	STATE_GIVECARD,				//カードを配布している
	STATE_BET,					//ポイントをベットする
	STATE_ENEMYTURN,			//敵のターン、思考している
	STATE_PLAYERTURN,			//プレイヤのターン
	STATE_HELP,					//ヘルプ画面を開く
	STATE_CHANGECARD_REMOVE,	//選んだカードを削除
	STATE_CHANGECARD_PULL,		//選んだカードを取得
	STATE_FOLD_OR_CALL,			//降りるか勝負するか決める
	STATE_OPEN,					//カードを開く
	STATE_GAMEJUDGE,			//この1戦の勝敗を決める
	STATE_TOTALJUDGE,			//最終的な勝敗を決める
	STATE_ONEROUND_FINISH,		//カードの削除など1戦ごとの終了処理
	STATE_RESULT,				//リザルトを表示している状態
	STATE_MAX
};

//勝敗結果
enum ROUND_RESULT {
	ROUND_RESULT_WIN_PC,
	ROUND_RESULT_WIN_NPC,
	ROUND_RESULT_FOLD_PC,
	ROUND_RESULT_FOLD_NPC,
	ROUND_RESULT_MAX
};

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int hPict_[3];			//画像番号
	Deck* pDeck_;
	Discards* pDiscards_;
	Player* pPlayerCharacter_;
	Player* pNonPlayerCharacter_;
	Log* pLog_;
	TotalResult* pTotalResult_;
	Help* pHelp_;
	Text* pText_;

	//false:初期化前 true:初期化後
	bool isStateInit_;

	//false:初期化前 true:初期化後
	bool isTurnInit_;

	//結果表示するか
	bool isResult_;

	//もう一度プレイするか
	bool isRetry_;

	bool isHelp_;
	int pict_;
	int helpPict_;
	bool model_;

	bool isEnd_;		//ゲーム終了か
	int betPoint_;		//各プレイヤの賭けポイントの合計
	int roundResult_;	//現在のラウンドの結果

	enum TURN
	{
		TURN_NON_PLAYER_CHARACTER,
		TURN_PLAYER_CHARACTER,
		TURN_ROUND_END,
		TURN_MAX
	} turnState_;

	/*enum CURSOR_POSITION
	{
		CURSOR_POSITION_CARD,
		CURSOR_POSITION_CHANGE,
		CURSOR_POSITION_HELP,
		CURSOR_POSITION_MAX
	} cursorPosition_;*/
	
	enum MOVE_STATE
	{
		MOVE_STATE_GATHER,
		MOVE_STATE_REMOVE,
		MOVE_STATE_MAX
	}moveState_;

	STATE state_;

	int selectCard_;

	std::vector<int> PCResult_;		//プレイヤーキャラの結果
	std::vector<int> NPCResult_;	//ノンプレイヤーキャラの結果

	int round_;						//現在のラウンド

public:
	//コンストラクタ
	//引数：parent  親オブジェクト
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//UpdateのmainStateを変更する
	//引数 state:変更先の状態
	void ChangeState(STATE state);

	//任意のカードのカーソルを表示
	void DisplayCursor(int selectCard);

	//勝敗判定
	//引数 pPCHand：プレイヤーキャラの手札, pNPCHand：ノンプレイヤーキャラの手札
	//戻り値 真：プレイヤーキャラの勝利, 偽：プレイヤーキャラの敗北
	/*bool DoJudge(Hand* pPCHand, Hand* pNPCHand);*/

	//勝敗判定
	//戻り値 真：プレイヤーキャラの勝利, 偽：プレイヤーキャラの敗北
	bool DoJudge();

	//リトライ
	void Retry();

	//賭けポイントを高いほうに合わせる
	void AdjustBetPoint();

	//勝敗結果をもとにポイントを勝者に与える
	void givePoint();

	//Getter
	Deck* GetDeck() { return pDeck_; }
	Discards* GetDiscards() { return pDiscards_; }
	Player* GetPlayerCharacter() { return pPlayerCharacter_; }
	Player* GetNonPlayerCharacter() { return pNonPlayerCharacter_; }
	Log* GetLog() { return pLog_; }
	STATE GetState() { return state_; }
};