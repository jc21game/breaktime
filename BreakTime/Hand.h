#pragma once
#include <queue>
#include <vector>
#include "Engine/IGameObject.h"
#include "CardData.h"

#define HAND_MAX 5				//手札の枚数

class Card;

//プレイヤー情報
enum CHARACTER {
	CHARACTER_PC,
	CHARACTER_NPC,
	CHARACTER_MAX
};

//アニメーション再生用情報
struct AnimationData {
	Card* pCard;
	CHARACTER charcter;
	int addPosition;
};

//強さ順でならべること！！
enum POKER_HAND {						//ポーカーの役
	POKER_HAND_HIGH,					//ハイカード
	POKER_HAND_ONE,						//ワンペア
	POKER_HAND_TWO,						//ツーペア
	POKER_HAND_THREE,					//スリーカード
	POKER_HAND_STRAIGHT,				//ストレート
	POKER_HAND_FLUSH,					//フラッシュ
	POKER_HAND_FULL_HOUSE,				//フルハウス
	POKER_HAND_FOUR,					//フォーカード
	POKER_HAND_STRAIGHT_FLUSH,			//ストレートフラッシュ
	POKER_HAND_ROYAL_FLUSH,				//ロイヤルフラッシュ
	POKER_HAND_MAX
};

//ペア情報
struct PAIR {
	int number;
	int count;

	PAIR(int number, int count)
	{
		this->number = number;
		this->count = count;
	}
};

//手札を管理するクラス
class Hand : public IGameObject
{
private:

	CardData* cardList_[HAND_MAX];			//持っているカード情報
	POKER_HAND pokerHand_;					//ポーカーの役
	int power_;								//手の強さ
	std::queue<AnimationData*> pPullList_;	//引くカード（アニメーション待ち）
	AnimationData* pPullingCard_;			//引いているカード（アニメーション再生中）
	std::list<PAIR>* pPairList_;

	D3DXVECTOR3 gatherPosition_;	//カードの集合位置
	bool initflag_;					//初期化
	

public:
	//コンストラクタ
	Hand(IGameObject* parent);

	//デストラクタ
	~Hand();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//手札にカードを追加
	//引数 cardData：カード情報, addPosition：位置
	void AddCard(CardData* cardData, int addPosition);

	//山札からカードを追加
	//引数 cardData：カード情報, addPosition：位置, character：プレイヤー
	void PullCard(CardData* cardData, int addPosition, CHARACTER character);

	//手札から任意のカードを取り除く
	//引数 removePosition：位置
	//戻り値：取り除いたカードの情報
	CardData* RemoveCard(int removePosition);

	//任意の位置のカード情報を取得
	CardData* GetCardListData(int position) { return cardList_[position]; }

	//カードを反転
	void TurnCards();

	//NPCのドローアニメーション再生
	void NPCPullAnimation(Card* pCard, int addPosition);

	//PCのドローアニメーション再生
	void PCPullAnimation(Card* pCard, int addPosition);

	//カードを非表示にする
	//引数	cardPosition:非表示にするカードの位置
	void HideCard(int cardPosition);

	void SetCardListDebug(CardData* data);

	//アクセス関数
	int GetPullListSize() { return pPullList_.size(); }

	//カードをまとめる
	void GatherCards();

	//カードの解散
	void ReverseCards();

	//カードが全員まとまったか
	bool IsGather();

	//カードが元の位置に戻ったか
	bool IsRemove();

	//pokerHand_に役を入れる、CalculatePowerを呼び出す
	void SetPokerHand();

	//手札の強さを算出、power_に入れる
	//引数 pHand：手札
	void CalculatePower(std::vector<CardData*>* pHand);

	//手札を強さ順に並べ替える
	//引数：pHand どのように並べ替えるか（並べ替えた結果）
	void SortHand();

	//一番多いSuitの数を返す
	int GetManySuitCnt();

	//一番多いsuitを返す
	int GetManySuit();

	//カードの最大値を返す
	int GetMaxCardNum();

	//カードの数値リストを返す
	std::vector<int> GetNumVec();

	//手札からペアリストを作成
	void CreatePairList();

	//役情報を返す
	POKER_HAND GetPokerHand();

	//ペアリストを返す
	std::list<PAIR>* GetPairList() { return pPairList_; }

	int GetPower() { return power_; }
};