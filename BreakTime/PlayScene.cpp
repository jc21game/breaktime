#include <vector>
#include <algorithm>
#include <iostream>
#include "PlayScene.h"
#include "Engine/Image.h"
#include "Engine/Model.h"
#include "Deck.h"
#include "Discards.h"
#include "Player.h"
#include "PlayerCharacter.h"
#include "NonPlayerCharacter.h"
#include "Hand.h"
#include "Table.h"
#include "TestCard.h"
#include "Log.h"
#include "BackBoard.h"
#include "TotalResult.h"
#include "BetBoard.h"
#include "Help.h"
#include "FoldOrCall.h"
#include "Emotion.h"
#include "Text.h"

//定数
const int ROUND_MAX = 5;		//最大ラウンド数

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), state_(STATE_SHUFFLE),  pDeck_(nullptr), pDiscards_(nullptr), pPlayerCharacter_(nullptr),
	pNonPlayerCharacter_(nullptr), pLog_(nullptr), pHelp_(nullptr), pText_(nullptr), isStateInit_(false), isTurnInit_(false), turnState_(TURN_NON_PLAYER_CHARACTER),
	selectCard_(0), isResult_(false), pict_(NULL), isHelp_(false), helpPict_(2), isRetry_(false), round_(1), isEnd_(false), betPoint_(0)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//初期化
void PlayScene::Initialize()
{
	CreateGameObject<BackBoard>(this);
	pDeck_ = CreateGameObject<Deck>(this);
	pDiscards_ = CreateGameObject<Discards>(this);
	pNonPlayerCharacter_ = CreateGameObject<NonPlayerCharacter>(this);
	pPlayerCharacter_ = CreateGameObject<PlayerCharacter>(this);
	CreateGameObject<Table>(this);
	pLog_ = CreateGameObject<Log>(this);
	pTotalResult_ = CreateGameObject<TotalResult>(this);
	pHelp_ = CreateGameObject<Help>(this);
	pText_ = CreateGameObject<Text>(this);

	//テスト(配置)
	//CreateGameObject<TestCard>(this);

	//画像データのロード
	hPict_[0] = Image::Load("data/Win.png");
	assert(hPict_[0] >= 0);
	Image::SetColor(hPict_[0], 1);
	hPict_[1] = Image::Load("data/Lose.png");
	assert(hPict_[1] >= 0);
	Image::SetColor(hPict_[1], 1);
	hPict_[2] = Image::Load("data/Help.png");
	assert(hPict_[2] >= 0);
	Image::SetColor(hPict_[2], 1);

}

//更新
void PlayScene::Update()
{
	//各プレイヤが勝負するかを保存
	bool npcCalled;
	bool playerCalled;

	//現在の状態に合わせて分岐
	switch (state_)
	{
	case STATE_SHUFFLE:
		//「ゲームスタート」
		pLog_->AddTemplate(TEMPLATE_START, NULL, NULL, NULL);
		//pText_->DisplayTemplate(TEXT_START);

		//if初期化されていないなら初期化
		if (!isStateInit_)
		{
			//アニメーションの設定

			isStateInit_ = true;
		}
		//if山札のアニメーションが終わった
			//状態をSTATE_GIVECARDに遷移する
			ChangeState(STATE_GIVECARD);
		break;
	case STATE_GIVECARD:
		if (!isStateInit_)
		{
			//turnState_の初期化
			turnState_ = TURN_NON_PLAYER_CHARACTER;
			isStateInit_ = true;
		}
		switch (turnState_)
		{
		//case敵に配っている
		case TURN_NON_PLAYER_CHARACTER:
			//if初期化されていないなら初期化
			if (!isTurnInit_)
			{
				//敵にカードを追加する
				pNonPlayerCharacter_->InitPullCard();
				//敵が自分の手の役と力を設定する
				pNonPlayerCharacter_->GetHand()->SetPokerHand();
				isTurnInit_ = true;
			}

			//ドローアニメーション終了か
			if (pNonPlayerCharacter_->GetHand()->GetPullListSize() == 0)
			{
				//ターンをプレイヤに
				turnState_ = TURN_PLAYER_CHARACTER;
				isTurnInit_ = false;
			}
			break;
		//caseプレイヤに配っている
		case TURN_PLAYER_CHARACTER:
			//if初期化されていないなら初期化
			if (!isTurnInit_)
			{
				//プレイヤにカードを追加する
				pPlayerCharacter_->InitPullCard();
				//プレイヤが自分の手の役と力を設定する
				pPlayerCharacter_->GetHand()->SetPokerHand();
				isTurnInit_ = true;
			}

			//ドローアニメーション終了か
			if (pPlayerCharacter_->GetHand()->GetPullListSize() == 0)
			{
				//ターンをエンドに
				turnState_ = TURN_ROUND_END;
				isTurnInit_ = false;
			}
			break;
		//case配り終わった
		case TURN_ROUND_END:
			//STATE_BETに遷移する
			ChangeState(STATE_BET);
			break;
		}
		break;
	case STATE_BET:
		//ベットのための初期化
		if (!isStateInit_)
		{
			//ベット用UIの表示
			((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->DisplayArrow(true);
			//NPCが手札を把握、狙う役を決める
			((NonPlayerCharacter*)pNonPlayerCharacter_)->SelectTargetHand();
		
			//NPCがベットするポイントを決める
			pNonPlayerCharacter_->betPoint();

			//NPCのリアクション（手札を把握したとき -> ベットポイント決定時）

			isStateInit_ = true;
		}

		//プレイヤがベットポイントを選択中
		if (pPlayerCharacter_->betPoint())
		{
			//選択終了
			ChangeState(STATE_ENEMYTURN);
			//ベット用UIの非表示
			((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->DisplayArrow(false);
		}


		break;
	case STATE_ENEMYTURN:
		//敵が自分の手から考える
		if (!isStateInit_)
		{
			//自分の手札にリアクションする(カードの内容を把握→そのカードに対しての感想)
			pNonPlayerCharacter_;
			//交換するカードを選択
			((NonPlayerCharacter*)pNonPlayerCharacter_)->SelectChangeCard();
			isStateInit_ = true;
		}

		if (isStateInit_)
		{
			ChangeState(STATE_PLAYERTURN);
		}

		break;
	case STATE_PLAYERTURN:
		//カーソルを表示
		DisplayCursor(selectCard_);

		//if[←]入力 && 左端ではない
		if (Input::IsKeyDown(DIK_LEFTARROW) && selectCard_ > 0)
		{
			//ひとつ左のトランプへ
			selectCard_--;
		}
		//if[→]入力 && 右端ではない
		if (Input::IsKeyDown(DIK_RIGHTARROW) && selectCard_ < HAND_MAX - 1)
		{
			//ひとつ右のトランプへ
			selectCard_++;
		}
		//if[↑]入力
		if (Input::IsKeyDown(DIK_UPARROW))
		{
			//選択に切り替え
			pPlayerCharacter_->ChangeSelectState(selectCard_, true);
		}
		//if[↓]入力
		if (Input::IsKeyDown(DIK_DOWNARROW))
		{
			//非選択に切り替え
			pPlayerCharacter_->ChangeSelectState(selectCard_, false);
		}
		//if[Enter]入力
		if (Input::IsKeyDown(DIK_RETURN))
		{
			//状態をSTATE_CHANGECARDに遷移する
			ChangeState(STATE_CHANGECARD_REMOVE);

			pPlayerCharacter_->HideCursor();
		}
		//if[Esc]入力
		if (Input::IsKeyDown(DIK_ESCAPE))
		{
			//状態をSTATE_HELPに遷移する
			ChangeState(STATE_HELP);
		}


		break;
	case STATE_HELP:
		//後で
		isHelp_ = true;
		pHelp_->MoveDis();

		if (Input::IsKeyDown(DIK_ESCAPE))
		{
			isHelp_ = false;
			pHelp_->MoveDis(true);
			ChangeState(STATE_PLAYERTURN);
		}
		break;
	case STATE_CHANGECARD_REMOVE:
		//if初期化されていないなら初期化
		if (!isStateInit_)
		{
			turnState_ = TURN_NON_PLAYER_CHARACTER;
			isStateInit_ = true;
		}
		switch (turnState_)
		{
		//case敵が捨てている
		case TURN_NON_PLAYER_CHARACTER:
			//敵の指定したカードを捨てる
			pNonPlayerCharacter_->DumpSelectCard();
			turnState_ = TURN_PLAYER_CHARACTER;
			break;
		//caseプレイヤが捨てている
		case TURN_PLAYER_CHARACTER:
			//プレイヤの指定したカードを捨てる
			pPlayerCharacter_->DumpSelectCard();
			turnState_ = TURN_ROUND_END;
			break;
		//case捨て終わった
		case TURN_ROUND_END:
			//STATE_ENEMYTURNに遷移する
			ChangeState(STATE_CHANGECARD_PULL);
			break;
		}
		break;
	case STATE_CHANGECARD_PULL:
		//if初期化されていないなら初期化
		if (!isStateInit_)
		{
			turnState_ = TURN_NON_PLAYER_CHARACTER;
			isStateInit_ = true;
		}
		switch (turnState_)
		{
			//case敵に配っている
		case TURN_NON_PLAYER_CHARACTER:
			if (!isTurnInit_)
			{
				moveState_ = MOVE_STATE_GATHER;
				isTurnInit_ = true;
				//敵の指定したカードに新しいデータを与える
				pNonPlayerCharacter_->PullSelectCard();
			}

			//ドローアニメーション終了か
			if (pNonPlayerCharacter_->GetHand()->GetPullListSize() == 0)
			{
				switch (moveState_)
				{
				case PlayScene::MOVE_STATE_GATHER:
					pNonPlayerCharacter_->GetHand()->GatherCards();

					if (pNonPlayerCharacter_->GetHand()->IsGather() == true)
					{
						//敵が自分の手の役と力を設定する
						pNonPlayerCharacter_->GetHand()->SetPokerHand();
						//手札を役の強さ順に並び替える
						pNonPlayerCharacter_->GetHand()->SortHand();


						moveState_ = MOVE_STATE_REMOVE;
					}
					break;

				case PlayScene::MOVE_STATE_REMOVE:

					pNonPlayerCharacter_->GetHand()->ReverseCards();

					if (pNonPlayerCharacter_->GetHand()->IsRemove() == true)
					{
						//ターンをプレイヤに
						turnState_ = TURN_PLAYER_CHARACTER;
						isTurnInit_ = false;
					}
					break;
				}
			}
			break;
			//caseプレイヤに配っている
		case TURN_PLAYER_CHARACTER:
			if (!isTurnInit_)
			{
				moveState_ = MOVE_STATE_GATHER;
				isTurnInit_ = true;

				//プレイヤの指定したカードに新しいデータを与える
				pPlayerCharacter_->PullSelectCard();
			}

			//アニメーション終了か
			if (pPlayerCharacter_->GetHand()->GetPullListSize() == 0)
			{
				switch (moveState_)
				{
				case PlayScene::MOVE_STATE_GATHER:
					pPlayerCharacter_->GetHand()->GatherCards();

					if (pPlayerCharacter_->GetHand()->IsGather() == true)
					{
						//敵が自分の手の役と力を設定する
						pPlayerCharacter_->GetHand()->SetPokerHand();
						pPlayerCharacter_->GetHand()->SortHand();

						moveState_ = MOVE_STATE_REMOVE;
					}
					break;

				case PlayScene::MOVE_STATE_REMOVE:

					pPlayerCharacter_->GetHand()->ReverseCards();

					if (pPlayerCharacter_->GetHand()->IsRemove() == true)
					{
						//ターンをプレイヤに
						turnState_ = TURN_ROUND_END;
						isTurnInit_ = false;
					}
					break;
				}
			}
			break;

			//case配り終わった
		case TURN_ROUND_END:
			//STATE_ENEMYTURNに遷移する
			ChangeState(STATE_FOLD_OR_CALL);
			break;
		}
		break;
	case STATE_FOLD_OR_CALL:
		if (!isStateInit_)
		{
			//NPCが降りるかどうか決める
			pNonPlayerCharacter_->SelectFoldOrCall();
			//リアクション？

			isStateInit_ = true;
		}

		//プレイヤが選択
		if (pPlayerCharacter_->SelectFoldOrCall())
		{
			//STATE_OPENに遷移
			ChangeState(STATE_OPEN);	
		}

		break;
	case STATE_OPEN:
		if (!isStateInit_)
		{
			//アニメーションの設定
			isStateInit_ = true;

			//各プレイヤが勝負するかを取得
			npcCalled = pNonPlayerCharacter_->GetIsCalled();
			playerCalled = pPlayerCharacter_->GetIsCalled();
			//プレイヤが勝負するか
			if (playerCalled)
			{
				//相手が勝負
				if (npcCalled)
				{
					//リアクション？

					//お互いのベットポイントを合わせる
					AdjustBetPoint();
				}
				//相手が降りる
				else
				{
					//リアクション？
				}
			}
			else
			{
				//リアクション？
			}

			//NPCカードを表に
			((Hand*)(pNonPlayerCharacter_->GetHand()))->TurnCards();
		}
		//if各プレイヤーのアニメーションが終わった
		//状態をSTATE_GAMEJUDGEに遷移する
		ChangeState(STATE_GAMEJUDGE);
		break;
	case STATE_GAMEJUDGE:
		//勝敗判定

		//各プレイヤーの賭けポイントを取得
		betPoint_ = ((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->GetBetPoint();
		betPoint_ += ((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->GetBetPoint();

		//各プレイヤが勝負するかを取得
		npcCalled = pNonPlayerCharacter_->GetIsCalled();
		playerCalled = pPlayerCharacter_->GetIsCalled();
		//両方とも勝負なら勝敗を判定する
		if (npcCalled && playerCalled)
		{
			//if勝ったのがプレイヤ
			if (DoJudge())
			{
				//プレイヤのWin関数を呼ぶ
				pPlayerCharacter_->WinPlayer();
				pict_ = 0;
				model_ = true;
				((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_WORRY);
				roundResult_ = ROUND_RESULT_WIN_PC;	
				//pText_->DisplayTemplate(TEXT_PLAYER_ROUND_WIN);
			}
			else
			{
				//NPCのWin関数を呼ぶ
				pNonPlayerCharacter_->WinPlayer();
				pict_ = 1;
				model_ = false;
				((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_FUNNY);
				roundResult_ = ROUND_RESULT_WIN_NPC;
				//pText_->DisplayTemplate(TEXT_NPC_ROUND_WIN);
			}
		}
		else
		{
			//プレイヤが降りた
			if (!playerCalled)
			{
				model_ = false;
				roundResult_ = ROUND_RESULT_FOLD_PC;
				((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_FUNNY);
				//ログに結果を表示
				pLog_->AddTemplate(TEMPLATE_ROUND, TEMPLATE_NPC, NULL, NULL);
				//pText_->DisplayTemplate(TEXT_NPC_ROUND_WIN);
			}
			//NPCが降りた
			else if (!npcCalled)
			{
				model_ = true;
				roundResult_ = ROUND_RESULT_FOLD_NPC;
				((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_WORRY);
				//ログに結果を表示
				pLog_->AddTemplate(TEMPLATE_ROUND, TEMPLATE_PC, NULL, NULL);
				//pText_->DisplayTemplate(TEXT_PLAYER_ROUND_WIN);
			}
		}
		

		//どちらかのポイントが0か確認
		isEnd_ = (((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->GetPoint() <= 0)
			|| (((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->GetPoint() <= 0);
		//現在のラウンドが最後
		if (round_ >= ROUND_MAX || isEnd_)
		{
			//STATE_TOTALJUDGEに遷移する
			ChangeState(STATE_TOTALJUDGE);
			isStateInit_ = false;
		}
		else
		{
			//STATE_ONEROUND_FINISHに遷移する
			ChangeState(STATE_ONEROUND_FINISH);
			isResult_ = true;
			isStateInit_ = false;
		}

		////ifPCかNPCのwin_が3になった
		//if (pPlayerCharacter_->GetWins() >= 3 || pNonPlayerCharacter_->GetWins() >= 3)
		//{
		//	//STATE_TOTALJUDGEに遷移する
		//	ChangeState(STATE_TOTALJUDGE);
		//}
		//else
		//{
		//	//STATE_ONEROUND_FINISHに遷移する
		//	ChangeState(STATE_ONEROUND_FINISH);
		//	isResult_ = true;
		//}

		break;
	case STATE_ONEROUND_FINISH:
		//float posX = pTotalResult_->GetPositionX();
		if (pTotalResult_->GetPositionX() < -15.0f || isRetry_)
		{	
			//初期化されていなかったら
			if (!isStateInit_)
			{
				//ポイントを勝者に与える
				givePoint();
				isStateInit_ = true;
			}


			if (Input::IsKeyDown(DIK_RETURN))
			{
				//終了処理
				pPlayerCharacter_->FinishDumpCard();
				pNonPlayerCharacter_->FinishDumpCard();
				//STATE_GIVECARDに遷移する
				ChangeState(STATE_GIVECARD);
				isStateInit_ = false;

				//NPCカードを裏に
				((Hand*)(pNonPlayerCharacter_->GetHand()))->TurnCards();
				
				//次のラウンドへ
				round_++;
			}
		}
		break;
	case STATE_TOTALJUDGE:
		//if初期化されていないなら初期化
		if (!isStateInit_)
		{
			//ポイントを勝者に与える
			givePoint();

			/*所持ポイントをもとに勝敗を決定*/
			//各プレイやのポイントを取得
			int playerPoint = ((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->GetPoint();
			int npcPoint = ((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->GetPoint();
			//ポイントを比べる
			if (playerPoint > npcPoint)
			{
				//ログに結果を表示
				pLog_->AddTemplate(TEMPLATE_GAME, TEMPLATE_PC, pPlayerCharacter_->GetWins(), pNonPlayerCharacter_->GetWins());

				//pText_->DisplayTemplate(TEXT_PLAYER_WIN);

				//WINの文字表示アニメーションの設定
				pTotalResult_->SetVictory(true);

				((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_SAD);
			}
			else if(npcPoint >= playerPoint)
			{
				//ログに結果を表示
				pLog_->AddTemplate(TEMPLATE_GAME, TEMPLATE_NPC, pPlayerCharacter_->GetWins(), pNonPlayerCharacter_->GetWins());

				//pText_->DisplayTemplate(TEXT_NPC_WIN);

				//LOSEの文字表示アニメーションの設定
				pTotalResult_->SetVictory(false);

				((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_FUNNY);
			}
			isStateInit_ = true;
		}
		//ifアニメーションのフレームが終わったら
			//STATE_RESULTに遷移
			ChangeState(STATE_RESULT);
			isStateInit_ = false;
		break;
	case STATE_RESULT:
		//入力処理
		isRetry_ = false;
		if (Input::IsKeyDown(DIK_R))
		{
			//シーンの関数にリトライ関数作成・ここで呼び出す
			Retry();

			//状態をSTATE_ONEROUND_FINISHに
			ChangeState(STATE_ONEROUND_FINISH);
			isRetry_ = true;
			isStateInit_ = false;

			//「ゲームスタート」
			pLog_->AddTemplate(TEMPLATE_START, NULL, NULL, NULL);
			//pText_->DisplayTemplate(TEXT_START);
		}
		
		break;
	default:
		break;
	}
}

//描画
void PlayScene::Draw()
{
	if (isResult_)
	{
		//Image::SetMatrix(hPict_[pict_], worldMatrix_);
		//Image::Draw(hPict_[pict_]);

		// モデルの動きを書き足したらこっちにします
		pTotalResult_->RoundResult(model_);
		isResult_ = false;
	}
	//if (isHelp_)
	//{
	//	D3DXMATRIX matPos, matScl;

	//	D3DXMatrixTranslation(&matPos, 100, 0, 0.93f);
	//	D3DXMatrixScaling(&matScl, 0.8, 0.7, 0.01f);

	//	matPos = matScl * matPos;

	//	Image::SetMatrix(hPict_[helpPict_], matPos);
	//	Image::Draw(hPict_[helpPict_]);
	//}
}

//解放
void PlayScene::Release()
{
}

void PlayScene::ChangeState(STATE state)
{
	// 状態の変更
	state_ = state;
	// 初期化フラグのリセット
	isStateInit_ = false;
}

//任意のカードのカーソルを表示
void PlayScene::DisplayCursor(int selectCard)
{
	pPlayerCharacter_->DisplayCursor(selectCard);
}

//勝敗判定
bool PlayScene::DoJudge()
{
	//デバッグ用カード操作
	{
		//	Hand* pPCDebug = CreateGameObject<Hand>(this);
		//	CardData pcData[HAND_MAX];
		//	pcData[0].number = 11;
		//	pcData[0].suit = SUIT_SPADE;
		//	pcData[1].number = 10;
		//	pcData[1].suit = SUIT_CLUB;
		//	pcData[2].number = 9;
		//	pcData[2].suit = SUIT_HEART;
		//	pcData[3].number = 8;
		//	pcData[3].suit = SUIT_SPADE;
		//	pcData[4].number = 7;
		//	pcData[4].suit = SUIT_CLUB;
		//	pPCDebug->SetCardListDebug(pcData);

		//	Hand* pNPCDebug = CreateGameObject<Hand>(this);
		//	CardData npcData[HAND_MAX];
		//	npcData[0].number = 13;
		//	npcData[0].suit = SUIT_HEART;
		//	npcData[1].number = 7;
		//	npcData[1].suit = SUIT_SPADE;
		//	npcData[2].number = 6;
		//	npcData[2].suit = SUIT_SPADE;
		//	npcData[3].number = 4;
		//	npcData[3].suit = SUIT_HEART;
		//	npcData[4].number = 3;
		//	npcData[4].suit = SUIT_SPADE;
		//	pNPCDebug->SetCardListDebug(npcData);

		//	//役の強さを算出
		//	int PCPower = JudgeHand(pPCDebug);
		//	int NPCPower = JudgeHand(pNPCDebug);

		//	//判定
		//	if (PCPower >= NPCPower)
		//	{
		//		return true;
		//	}
	}

	

	//役の強さを算出
	int PCPower = pPlayerCharacter_->GetHand()->GetPower();
	int NPCPower = pNonPlayerCharacter_->GetHand()->GetPower();

	//Powerから役のみを取り出す（17はPOKER_HANDとTEMPLATEのenumの差）
	int NPCHand = (NPCPower >> 20) + 17;
	int PCHand = (PCPower >> 20) + 17;

	//各プレイヤの役をログに表示
	pLog_->AddTemplate(NPCHand, TEMPLATE_HAND_NPC, NULL, NULL);
	pLog_->AddTemplate(PCHand, TEMPLATE_HAND_PC, NULL, NULL);

	//役を保存
	PCResult_.push_back(PCPower);
	NPCResult_.push_back(NPCPower);

	//判定
	//PCの勝ち
	if (PCPower >= NPCPower)
	{
		//ログに勝敗を表示
		pLog_->AddTemplate(TEMPLATE_ROUND, TEMPLATE_PC, NULL, NULL);
		return true;
	}
	//NPCの勝ち
	else
	{
		//ログに勝敗を表示
		pLog_->AddTemplate(TEMPLATE_ROUND, TEMPLATE_NPC, NULL, NULL);
		return false;
	}

}

void PlayScene::Retry()
{
	//PC、NPCそれぞれの勝ち数初期化
	pPlayerCharacter_->ResetWins();
	pNonPlayerCharacter_->ResetWins();

	//PC、NPCそれぞれのポイントを初期化
	pPlayerCharacter_->ResetPoint();
	pNonPlayerCharacter_->ResetPoint();

	//ラウンド数初期化
	round_ = 1;

	//ゲームログのリストを初期化
	pLog_->Release();

	//NPCの表情を初期化
	((NonPlayerCharacter*)pNonPlayerCharacter_)->SetEmotion(EMOTION_STANDARD);

	//表示初期化
	pTotalResult_->Reset();
}

void PlayScene::AdjustBetPoint()
{
	//各プレイヤ賭けポイントを取得
	int npcPoint = ((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->GetBetPoint();
	int playerPoint = ((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->GetBetPoint();

	//npc賭けポイントのほうが多い
	if (npcPoint > playerPoint)
	{
		//プレイヤの賭けポイントをnpcに合わせる
		((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->AddBetPoint(npcPoint - playerPoint);
	}
	//プレイヤ賭けポイントのほうが多い
	else if (playerPoint > npcPoint)
	{
		//npcの賭けポイントをプレイヤに合わせる
		((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->AddBetPoint(playerPoint - npcPoint);
	}
}

void PlayScene::givePoint()
{
	switch (roundResult_)
	{
	case ROUND_RESULT_WIN_PC:
	case ROUND_RESULT_FOLD_NPC:
		//プレイヤに賭けポイントを与える
		((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->AddPoint(betPoint_);
		break;

	case ROUND_RESULT_WIN_NPC:
	case ROUND_RESULT_FOLD_PC:
		//NPCに賭けポイントを与える
		((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->AddPoint(betPoint_);
		break;
	}

	//賭けポイントをクリア
	((BetBoard*)(pPlayerCharacter_->GetBetBoard()))->SetBetPoint(0);
	((BetBoard*)(pNonPlayerCharacter_->GetBetBoard()))->SetBetPoint(0);
	betPoint_ = 0;
}
