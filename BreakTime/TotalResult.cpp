#include "TotalResult.h"
#include "Engine/Model.h"

//コンストラクタ
TotalResult::TotalResult(IGameObject * parent)
	:IGameObject(parent, "TotalResult"), result_(ISVICTORY_MAX), vicAction_(VICTORY_ACTION_INIT), defAction_(DEFEAT_ACTION_INIT),
	speed_(0.2f), isTotal_(false), isRound_(false), stop_(1), start_(0), now_(0)
{
	for (int i = 0; i < ISVICTORY_MAX; i++)
	{
		hModel_[i] = -1;
	}
	position_.y = 5.0f;
	position_.x = 80.0f;
	position_.z = -0.0f;
}

//デストラクタ
TotalResult::~TotalResult()
{
}

//初期化
void TotalResult::Initialize()
{
	//モデルハンドル作成
	hModel_[ISVICTORY_LOSE] = Model::Load("data/GameLose.fbx");
	hModel_[ISVICTORY_WIN] = Model::Load("data/GameWin.fbx");


}

//更新
void TotalResult::Update()
{
}

//描画
void TotalResult::Draw()
{
	if (isTotal_)
	{
		switch (result_)
		{
		case ISVICTORY_WIN:
			VictoryMove();
			break;
		case ISVICTORY_LOSE:
			DefeatMove();
			break;
		}
	}
	if(isRound_)
	{
		position_.x -= speed_;
		if (-0.1f < position_.x && position_.x < 0.1f)
		{
			time(&start_);
			while (true)
			{
				time(&now_);
				double diff = difftime(now_, start_);
				if (diff > stop_)
				{
					break;
				}
			}
		}
		if (position_.x < -15.0f)
		{
			isRound_ = false;
			Reset();
		}
		
	}

	switch (result_)
	{
	case ISVICTORY_WIN:
		Model::SetMatrix(hModel_[ISVICTORY_WIN], worldMatrix_);
		Model::Draw(hModel_[ISVICTORY_WIN]);
		break;
	case ISVICTORY_LOSE:
		Model::SetMatrix(hModel_[ISVICTORY_LOSE], worldMatrix_);
		Model::Draw(hModel_[ISVICTORY_LOSE]);
		break;
	default:
		break;
	}
}

//開放
void TotalResult::Release()
{
}

void TotalResult::SetVictory(bool isVictory)
{

	isTotal_ = true;

	if (isVictory)
	{
		result_ = ISVICTORY_WIN;
	}
	else
	{
		result_ = ISVICTORY_LOSE;

	}
}

void TotalResult::Reset()
{
	result_ = ISVICTORY_MAX;
	vicAction_ = VICTORY_ACTION_INIT;
	defAction_ = DEFEAT_ACTION_INIT;
	isRound_ = false;
	isTotal_ = false;

	position_.y = 5.0f;
	position_.x = -80.0f;
	position_.z = -0.0f;
}

void TotalResult::RoundResult(bool isVictory)
{
	position_.y = 5.0f;
	position_.x = 15.0f;
	position_.z = -0.0f;

	isRound_ = true;

	if (isVictory)
	{
		result_ = ISVICTORY_WIN;
		scale_.x = 0.2f;
		scale_.y = 0.2f;
		scale_.z = 0.2f;
	}
	else
	{
		result_ = ISVICTORY_LOSE;
		scale_.x = 0.1f;
		scale_.y = 0.1f;
		scale_.z = 0.1f;
	}

}

void TotalResult::VictoryMove()
{
	switch(vicAction_)
	{
	case VICTORY_ACTION_INIT:
		position_.z = -10;
		position_.y = 3.5f;
		position_.x = 0;
		scale_.x = 0.01f;
		scale_.y = 0.01f;
		scale_.z = 0.01f;
		vicAction_ = VICTORY_ACTION_FALL;
		break;
	case VICTORY_ACTION_FALL:
		scale_.x += 0.002f;
		scale_.y += 0.002f;
		scale_.z += 0.002f;
		if (0.2f < scale_.x)
		{
			vicAction_ = VICTORY_ACTION_LAND;
		}
		break;


	}
}

void TotalResult::DefeatMove()
{
	switch (defAction_)
	{
	case DEFEAT_ACTION_INIT:
		position_.z = -10;
		position_.y = 3.5f;
		position_.x = 0;
		scale_.x = 0.01f;
		scale_.y = 0.01f;
		scale_.z = 0.01f;

		defAction_ = DEFEAT_ACTION_FALL;
		break;
	case DEFEAT_ACTION_FALL:
		scale_.x += 0.002f;
		scale_.y += 0.002f;
		scale_.z += 0.002f;
		if (0.1f < scale_.x)
		{
			defAction_ = DEFEAT_ACTION_LAND;
		}
		
		break;
	}
}
