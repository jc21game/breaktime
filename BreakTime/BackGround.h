#pragma once
#include "Engine/IGameObject.h"


class TitleImage;

//背景を管理するクラス
class BackGround : public IGameObject
{
private:
	int hPict_;

	float clear_;		// α値更新
	float stride_;		// ↑の変化式
	TitleImage* pTitleImage_;		//タイトル

	bool isLoad_;		//背景の表示が終了したか

public:
	//コンストラクタ
	BackGround(IGameObject* parent);

	//デストラクタ
	~BackGround();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//アクセス関数
	float Getclear() { return clear_; }
	bool GetLoadFlag_() { return isLoad_; }
};