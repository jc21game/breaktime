#include "SplashScene.h"
#include "Engine/Image.h"
#include "School.h"
#include "TeamLogo.h"
#include "TeamRing.h"

//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"),		//親のコンストラクタを呼び出す
	currentLogo_(SPLASH_LOGO_SCHOOL)
{
}

//初期化
void SplashScene::Initialize()
{
	CreateGameObject<School>(this);
}

//更新
void SplashScene::Update()
{
	//学校名表示し終わったら
	if (childList_.empty())
	{
		MoveNext(false);
	}
	

}

//描画
void SplashScene::Draw()
{
}

//解放
void SplashScene::Release()
{
}

//次に移る
void SplashScene::MoveNext(bool flg)
{
	switch (currentLogo_)
	{
	case SPLASH_LOGO_SCHOOL:
		{
			Teamlogo* pTeamlogo = CreateGameObject<Teamlogo>(this);
			if (flg)
			{
				pTeamlogo->SetAlpha(1.0f);
			}
			currentLogo_ = SPLASH_LOGO_TEAM;
			break;
		}
	case SPLASH_LOGO_TEAM:		
		SceneManager::ChangeScene(SCENE_ID_TITLE);
		break;
	default:
		break;
	}
}


