#include "NonPlayerCharacter.h"
#include "Engine/Image.h"
#include "Card.h"
#include "Hand.h"
#include "Deck.h"
#include "PlayScene.h"
#include "StandardAI.h"
#include "Engine/Model.h"
#include "BetBoard.h"

#define ADD_POINT		10			//ポイント増減の単位
#define BET_HEIGHT		-15 		//賭けポイント表示の高さ
#define BET_WIDTH		200			//賭けポイント表示の幅
#define POINT_HEIGHT	 65			//所持ポイント表示の高さ
#define POINT_WIDTH		110			//所持ポイント表示の幅

NonPlayerCharacter::NonPlayerCharacter(IGameObject * parent) : Player(parent), hModel_(-1)
{
	pAi_ = new StandardAI(this);

	//初期化
	for (int i = 0; i < EMOTION_MAX; i++)
	{
		hPicts_[i] = -1;
	}
}


NonPlayerCharacter::~NonPlayerCharacter()
{
	SAFE_DELETE(pAi_);
}

void NonPlayerCharacter::InitHandCardsPos()
{
	//手札位置設定
	float x = -2.9f;
	for (int i = 0; i < HAND_MAX; i++)
	{
		pHand_->GetChild(i)->SetPosition(D3DXVECTOR3(x, 0, 1));
		x += 1.2f;
	}

	//NPCモデルをロード
	/*hModel_ = Model::Load("Data/zunko/Zunko.fbx", true);
	assert(hModel_ >= 0);*/
	
	//画像データをロード
	std::string filePath[EMOTION_MAX];
	filePath[EMOTION_STANDARD] = "kirakira";
	filePath[EMOTION_LOOK] = "ossu";
	filePath[EMOTION_FUNNY] = "wa-i_b";
	filePath[EMOTION_SAD] = "naki";
	filePath[EMOTION_ANGER] = "gyaa";
	filePath[EMOTION_WORRY] = "gaaan_b";
	filePath[EMOTION_SURPRISED] = "bikkurii_b";

	for (int i = 0; i < EMOTION_MAX; i++)
	{
		hPicts_[i] = Image::Load("Data/zunko/" + filePath[i] + ".png");
		assert(hPicts_[i] > 0);
		Image::SetColor(hPicts_[i], 1.0f);
	}

}

void NonPlayerCharacter::InitBetBoardPos()
{
	pBetBoard_->SetBetPosition(D3DXVECTOR3(BET_WIDTH, BET_HEIGHT, 1.0f));
	pBetBoard_->SetPointPosition(D3DXVECTOR3(POINT_WIDTH, POINT_HEIGHT, 1.0f));
}


void NonPlayerCharacter::Draw()
{
	//D3DXMATRIX m;
	//D3DXMatrixTranslation(&m, 740, 100, 0);

	////勝数描画
	//Image::SetMatrix(hPict_[wins_], worldMatrix_ * m);
	//Image::Draw(hPict_[wins_]);

	//モデル描画
	/*Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);*/

	//NPC画像描画
	D3DXMATRIX mat, matT, matS;
	D3DXMatrixScaling(&matS, 0.2f, 0.2f, 1.0f);
	D3DXMatrixTranslation(&matT, 300, 250, 1.0f);
	mat = matS * matT;
	int emotion = pAi_->GetEmotion();
	Image::SetMatrix(hPicts_[emotion], mat);
	Image::Draw(hPicts_[emotion]);
}

void NonPlayerCharacter::PullCard(int addPosition)
{
	CardData* pull = ((PlayScene*)pParent_)->GetDeck()->PullCard();
	pHand_->PullCard(pull, addPosition, CHARACTER_NPC);
}

void NonPlayerCharacter::SelectTargetHand()
{
	pAi_->SelectTargetHand();
}

void NonPlayerCharacter::SelectChangeCard()
{
	pAi_->SelectChangeCard();
}

bool NonPlayerCharacter::betPoint()
{
	pAi_->DecideBetPoint();
	return true;
}

bool NonPlayerCharacter::SelectFoldOrCall()
{
	//勝負するかを保存
	isCalled_ = pAi_->FoldOrCall();
	
	//選択終了
	return true;
}

void NonPlayerCharacter::SetEmotion(EMOTION emotion)
{
	pAi_->SetEmotion(emotion);
}
