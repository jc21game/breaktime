#pragma once
#include "Engine/IGameObject.h"



//チームロゴの外側を管理するクラス
class TeamRing : public IGameObject
{
	enum FADE {
		FADE_IN,
		FADE_OUT
	}fade_;


	int hPict_;
	float alpha_;
	const float spinSpeed_ ;
	const float shrinkSpeed_;
	float shrink_;

public:
	//コンストラクタ
	TeamRing(IGameObject* parent);

	//デストラクタ
	~TeamRing();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetAlpha(float alpha) { alpha_ = alpha; };

	D3DXMATRIX RotationPict();

};