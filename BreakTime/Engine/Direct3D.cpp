#include "Global.h"

//初期化
LPDIRECT3D9			Direct3D::pD3d = nullptr;
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;

//Direct3Dの初期化
void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);			//インタフェースはnewできないので専用の関数を使用
	assert(pD3d != nullptr);							//エラー処理

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;						//専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));					//中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;								//ウィンドウかフルスクリーンか(true:ウィンドウ)
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;				//幅
	d3dpp.BackBufferHeight = g.screenHeight;			//高さ
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);
	assert(pDevice != nullptr);							//エラー処理


	//アルファブレンド
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);		//TRUEがデフォルトなので記述しなくてもいい

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));		//すべて0で初期化
	//必要な項目のみを設定
	lightState.Type = D3DLIGHT_DIRECTIONAL;				//種類
	lightState.Direction = D3DXVECTOR3(1, -1, 1);		//向き
	lightState.Diffuse.r = 1.0f;						//色（R）
	lightState.Diffuse.g = 1.0f;						//色（G）
	lightState.Diffuse.b = 1.0f;						//色（B）
	lightState.Ambient.r = 1.0f;						//アンビエント(R)
	lightState.Ambient.g = 1.0f;						//アンビエント(G)
	lightState.Ambient.b = 1.0f;						//アンビエント(B)
	//ライトを作成	 //ライト番号
	pDevice->SetLight(0, &lightState);
	//ライトをONにする
	pDevice->LightEnable(0, TRUE);

	//カメラ
	D3DXMATRIX view, proj;			//ビュー行列,プロジェクション行列
					  //格納先		//視点				//焦点（注視点）		//カメラの上の位置
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));			//ビュー行列作成
							  //格納先	//視野角		//アスペクト比（縦横比）		//描画手前位置	//描画最奥位置
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 200.0f);	//プロジェクション行列作成
	pDevice->SetTransform(D3DTS_VIEW, &view);			//ビュー行列をセット
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);		//プロジェクション行列をセット

}

//描画開始
void Direct3D::BeginDraw()
{
	//画面をクリア
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

//描画終了
void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

//解放
void Direct3D::Release()
{
	//解放処理
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pD3d);
}
