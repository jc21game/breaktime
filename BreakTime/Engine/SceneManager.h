#pragma once
#include "IGameObject.h"

enum SCENE_ID
{
	SCENE_ID_PLAY,
	SCENE_ID_TITLE,
	SCENE_ID_MENU,
	SCENE_ID_SPLASH,
};

//シーンマネージャーを管理するクラス
class SceneManager : public IGameObject
{
	static SCENE_ID currentSceneID_;		//現在のシーン
	static SCENE_ID nextSceneID_;			//次のシーン
	static IGameObject* pCurrentScene_;		//現在のシーンのアドレス

public:
	//コンストラクタ
	//引数：parent  親オブジェクト
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//シーンの切り替え
	static void ChangeScene(SCENE_ID next);

	//静的メンバ関数
	static IGameObject* GetCurrentScene() { return pCurrentScene_; }
};