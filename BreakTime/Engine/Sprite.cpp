#include "Sprite.h"
#include "Direct3D.h"

Sprite::Sprite() :
	pSprite_(nullptr), pTexture_(nullptr)	//変数生成と同時に初期化される
{
}

Sprite::~Sprite()
{
	//解放処理
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}

//画像ファイルの読み込み
void Sprite::Load(const char *pPath)
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);
	assert(pSprite_ != nullptr);					//エラー処理

	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, pPath,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);					//エラー処理
}

//描画
void Sprite::Draw(const D3DXMATRIX &matrix)
{
	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, D3DXCOLOR(1, 1, 1, alpha_));
	pSprite_->End();
}

//テクスチャのサイズを取得
D3DXVECTOR2 Sprite::GetTextureSize()
{
	D3DXVECTOR2 size;
	D3DSURFACE_DESC d3dds;
	pTexture_->GetLevelDesc(0, &d3dds);
	size.x = d3dds.Width;
	size.y = d3dds.Height;

	return size;
}

void Sprite::Setcolor(float alpha)
{
	alpha_ = alpha;
}