#pragma once
#include "Global.h"

class Collider
{
	IGameObject* owner_;			//このコライダーを付けたオブジェクト
	D3DXVECTOR3 center_;			//プレイヤーから見た中心位置
	float		radius_;			//半径

public:
	//引数ありコンストラクタ
	//引数：owner	コライダーをつけるオブジェクト
	//引数：center	オブジェクトから見た中心位置
	//引数：radius	半径
	Collider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	~Collider();

	//当たり判定
	//引数：target	判定対象
	bool IsHit(Collider* target);
};

