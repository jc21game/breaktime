#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "Input.h"
#include "Direct3D.h"
#include "IGameObject.h"
#include "SceneManager.h"

//解放処理
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

struct Global
{
	int screenWidth;		//ウィンドウの縦幅
	int screenHeight;		//ウィンドウの横幅
	HWND hWnd;				//ウィンドウハンドル
};
extern Global g;