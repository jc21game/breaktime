#pragma once
#include <xact3.h>

namespace Audio
{

	//初期化
	void Initialize();

	//ウェーブバンクをロード
	//引数：waveBank	ファイルパス
	void LoadWaveBank(char* waveBank);

	//サウンドバンクをロード
	//引数：soundBank	ファイルパス
	void LoadSoundBank(char* soundBank);

	//再生
	//引数：cueName		サウンド名
	void Play(char* cueName);

	//停止
	//引数：cueName		サウンド名
	void Stop(char* cueName);

	//開放
	void Release();
};
