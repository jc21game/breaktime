#pragma once
#define DIRECTINPUT_VERSION 0x0800

#include <dInput.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

//Global.hをincludeできないので記述（GlobalにInput.hをincludeするため）
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	//初期化
	//引数：hWnd	ウィンドウハンドル
	void Initialize(HWND hWnd);
	
	//更新
	void Update();
	
	//解放
	void Release();

	/////////////////////////// キーボード //////////////////////////////////
	//キーが押されたか
	//引数：keyCode		調べたいキー
	bool IsKey(int keyCode);
	
	//今キーを押したか
	//引数：keyCode		調べたいキー
	bool IsKeyDown(int keyCode);
	
	//今キーを放したか
	//引数：keyCode		調べたいキー
	bool IsKeyUp(int keyCode);


	/////////////////////////// マウス //////////////////////////////////
	//マウスのボタンが押されているか調べる
	//引数：buttonCode 調べたいボタンの番号
	//戻値：押されていればtrue
	bool IsMouseButton(int buttonCode);

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：buttonCode 調べたいボタンの番号
	//戻値：押した瞬間だったらtrue
	bool IsMouseButtonDown(int buttonCode);

	//マウスのボタンを今放したか調べる
	//引数：buttonCode 調べたいボタンの番号
	//戻値：放した瞬間だったらtrue
	bool IsMouseButtonUp(int buttonCode);


	//マウスカーソルの位置を取得
	//戻値：マウスカーソルの位置
	D3DXVECTOR3 GetMousePosition();

	//そのフレームでのマウスの移動量を取得
	//戻値：X,Y マウスの移動量 ／ Z,ホイールの回転量
	D3DXVECTOR3 GetMouseMove();
	
};