#include "Quad.h"
#include "Direct3D.h"



Quad::Quad() :
	pVertexBuffer_(nullptr),
	pIndexBuffer_(nullptr),
	pTexture_(nullptr),
	material_({ 0 }),
	vertex_(0),
	polygon_(0)
{
}


Quad::~Quad()
{
	//逆順で解放
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

//準備
void Quad::Load(const char * filename)
{
	//頂点情報
	Vertex vertexList[] = {
		//位置					//法線					//uv座標
		D3DXVECTOR3(-1, 1, 0),  D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(0, 0),
		D3DXVECTOR3(1, 1, 0),   D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(1, 0),
		D3DXVECTOR3(1, -1, 0),  D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(1, 1),
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(0, 1)
	};
	//頂点数指定
	vertex_ = sizeof(vertexList) / sizeof(Vertex);

	//インデックス情報
	int indexList[] = { 0, 2, 3, 0, 1, 2 };

	//ポリゴン数取得
	polygon_ = ( sizeof(indexList) / sizeof(int) ) / 3;

	//各情報の準備
	LoadVertex(vertexList, sizeof(vertexList));
	LoadTexture(filename);
	LoadIndex(indexList, sizeof(indexList));
}

//頂点情報の準備
void Quad::LoadVertex(Vertex *pVertex, UINT length)
{
	//頂点バッファ確保
	Direct3D::pDevice->CreateVertexBuffer(length, 0,
		//位置		//法線			//uv座標					   //ここにアドレスが入る
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);

	//エラー処理
	assert(pVertexBuffer_ != nullptr);

	//頂点バッファ(バーテックスバッファ)に頂点情報を格納
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);		//ロック
	memcpy(vCopy, pVertex, length);						//コピー
	pVertexBuffer_->Unlock();							//アンロック
}

//テクスチャの準備とマテリアルの設定
void Quad::LoadTexture(const char *filename)
{
	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, filename,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);					//エラー処理


	//マテリアルの設定（色の反射する割合）
	material_.Diffuse.r = 1.0f;
	material_.Diffuse.g = 1.0f;
	material_.Diffuse.b = 1.0f;
	//アンビエントの設定（影の濃さの調整）
	material_.Ambient.r = 0.2f;
	material_.Ambient.g = 0.2f;
	material_.Ambient.b = 0.2f;
}

//インデックス情報の準備
void Quad::LoadIndex(int *indexList, UINT length)
{
	//インデックスバッファ確保
	Direct3D::pDevice->CreateIndexBuffer(length, 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);

	//インデックスバッファにインデックス情報を格納
	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);		//ロック
	memcpy(iCopy, indexList, length);					//コピー
	pIndexBuffer_->Unlock();							//アンロック
}

//表示
void Quad::Draw(const D3DXMATRIX &matrix)
{
	//変形を指定
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);
	//テクスチャを指定			 //テクスチャ番号
	Direct3D::pDevice->SetTexture(0, pTexture_);
	//マテリアルを指定
	Direct3D::pDevice->SetMaterial(&material_);
	//頂点バッファを指定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	//インデックスバッファを指定
	Direct3D::pDevice->SetIndices(pIndexBuffer_);
	//どんな情報が入っているか指定
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);
	//描画
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertex_, 0, polygon_);
}
