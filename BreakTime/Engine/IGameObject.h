#pragma once
#include <d3dx9.h>
#include <list>
#include <string>

class Collider;

class IGameObject
{
protected:
	IGameObject* pParent_;					//親
	std::list<IGameObject*> childList_;		//子のリスト
	std::string name_;						//オブジェクトの名前

	D3DXVECTOR3 position_;					//位置
	D3DXVECTOR3 rotate_;					//向き
	D3DXVECTOR3 scale_;						//拡大率

	D3DXMATRIX localMatrix_;				//オブジェクトの行列（親から見た）
	D3DXMATRIX worldMatrix_;				//親の影響を受けた行列

	bool dead_;								//死亡フラグ

	Collider* pCollider_;					//コライダー

	//変形
	void Transform();

public:
	//コンストラクタ
	IGameObject();
	//引数:	parent	親
	IGameObject(IGameObject* parent);
	//引数:	parent	親		name	オブジェクト名
	IGameObject(IGameObject* parent, const std::string& name);

	//仮想デストラクタ
	virtual ~IGameObject();

	//初期化
	virtual void Initialize() = 0;

	//更新
	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//解放
	virtual void Release() = 0;

	//何かと衝突した場合
	//引数：pTarget	衝突した相手
	virtual void OnCollision(IGameObject* pTarget) {};


	//子と自分の更新を呼ぶ
	void UpdateSub();

	//子と自分の描画を呼ぶ
	void DrawSub();

	//子と自分の解放を呼ぶ
	void ReleaseSub();

	//自分を削除する
	void KillMe();

	//コライダーを設定
	//引数：center	中心位置	radius	半径
	void SetCollider(D3DXVECTOR3& center, float radius);

	//対象オブジェクト以下の衝突処理
	//引数：targetObject	対象とするオブジェクト
	void Collision(IGameObject* targetObject);

	//オブジェクト生成
	//引数:	parent	親
	template<class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//引数のparentを親としてオブジェクトを生成
		T* p = new T(parent);
		//parentに自分を子として登録
		parent->PushBackChild(p);
		//更新
		p->Initialize();

		return p;
	}

	//子のリストに追加
	//引数:	pObj	追加するオブジェクト
	void PushBackChild(IGameObject* pObj);

	//子のリストに存在するか
	bool IsExistInChildList(std::string& objectName);

	//セッター
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
	void SetPositionX(float x) { position_.x = x; }
	void SetPositionY(float y) { position_.y = y; }
	void SetPositionZ(float z) { position_.z = z; }
	void SetRotate(D3DXVECTOR3 rotate) { rotate_ = rotate; }
	void SetScale(D3DXVECTOR3 scale) { scale_ = scale; }

	//ゲッター
	D3DXVECTOR3 GetPosition() { return position_; }
	D3DXVECTOR3 GetRotate() { return rotate_; }
	D3DXVECTOR3 GetScale() { return scale_; }
	std::string GetName() { return name_; }
	IGameObject* GetChild(int ID);
	IGameObject* GetParent() {return pParent_; }
};

