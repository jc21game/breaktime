#include "Camera.h"
#include "Global.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(D3DXVECTOR3(0, 0, 1))
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	D3DXMATRIX proj;			//プロジェクション行列
							  //格納先	//視野角		//アスペクト比（縦横比）		//描画手前位置	//描画最奥位置
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 200.0f);	//プロジェクション行列作成
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);		//プロジェクション行列をセット
}

//更新
void Camera::Update()
{
	Transform();

	//ビュー行列変更
	D3DXVECTOR3 worldPosition;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVECTOR3 worldTarget;
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);
	D3DXMATRIX view;
					//格納先	//視点	//焦点（注視点）//カメラの上の位置
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));			//ビュー行列作成
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);		//ビュー行列をセット
}

//描画
void Camera::Draw()
{
}

//解放
void Camera::Release()
{
}