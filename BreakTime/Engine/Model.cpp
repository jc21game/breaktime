#include "Model.h"

//3Dモデル（FBXファイル）を管理する
namespace Model
{
	//ロード済みのモデルデータ一覧
	std::vector<ModelData*>	datas_;

	//初期化
	void Initialize()
	{
		AllRelease();
	}

	//モデルをロード
	int Load(std::string fileName, bool unipue)
	{
		ModelData* pData = new ModelData;


		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;
		for (int i = 0; i < datas_.size(); i++)
		{
			//すでに開いている場合
			if (datas_[i] != nullptr && datas_[i]->fileName == fileName && !unipue)
			{
				pData->pFbx = datas_[i]->pFbx;
				isExist = true;
				break;
			}
		}

		//新たにファイルを開く
		if (isExist == false)
		{
			pData->pFbx = new Fbx;
			if (FAILED(pData->pFbx->Load(fileName)))
			{
				//開けなかった
				SAFE_DELETE(pData->pFbx);
				SAFE_DELETE(pData);
				return -1;
			}

			//無事開けた
			pData->fileName = fileName;
		}


		//使ってない番号が無いか探す
		for (int i = 0; i < datas_.size(); i++)
		{
			if (datas_[i] == nullptr)
			{
				datas_[i] = pData;
				return i;
			}
		}

		//新たに追加
		datas_.push_back(pData);
		return datas_.size() - 1;
	}

	//モデルをロード
	int Load(std::string fileName)
	{
		return Load(fileName, false);
	}



	//描画
	void Draw(int handle)
	{
		if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
		{
			return;
		}

		//アニメーションを進める
		datas_[handle]->nowFrame += datas_[handle]->animSpeed;

		//最後までアニメーションしたら戻す
		if (datas_[handle]->nowFrame > datas_[handle]->endFrame)
			datas_[handle]->nowFrame = datas_[handle]->startFrame;



		if (datas_[handle]->pFbx)
		{
			datas_[handle]->pFbx->Draw(datas_[handle]->matrix, datas_[handle]->nowFrame);
		}
	}


	//任意のモデルを開放
	void Release(int handle)
	{
		if (handle < 0 || handle >= datas_.size() || datas_[handle] == nullptr)
		{
			return;
		}

		//同じモデルを他でも使っていないか
		bool isExist = false;
		for (int i = 0; i < datas_.size(); i++)
		{
			//すでに開いている場合
			if (datas_[i] != nullptr && i != handle && datas_[i]->pFbx == datas_[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		//使ってなければモデル解放
		if (isExist == false)
		{
			SAFE_DELETE(datas_[handle]->pFbx);
		}


		SAFE_DELETE(datas_[handle]);
	}


	//全てのモデルを解放
	void AllRelease()
	{
		for (int i = 0; i < datas_.size(); i++)
		{
			if (datas_[i] != nullptr)
			{
				Release(i);
			}
		}
		datas_.clear();
	}


	//アニメーションのフレーム数をセット
	void SetAnimFrame(int handle, int startFrame, int endFrame, float animSpeed)
	{
		datas_[handle]->SetAnimFrame(startFrame, endFrame, animSpeed);
	}


	//現在のアニメーションのフレームを取得
	int GetAnimFrame(int handle)
	{
		return datas_[handle]->nowFrame;
	}


	//任意のボーンの位置を取得
	D3DXVECTOR3 GetBonePosition(int handle, std::string boneName)
	{
		D3DXVECTOR3 pos = datas_[handle]->pFbx->GetBonePosition(boneName);
		D3DXVec3TransformCoord(&pos, &pos, &datas_[handle]->matrix);
		return pos;
	}


	//ワールド行列を設定
	void SetMatrix(int handle, D3DXMATRIX matrix)
	{
		if (handle < 0 || handle >= datas_.size())
		{
			return;
		}

		datas_[handle]->matrix = matrix;
	}


	//ワールド行列の取得
	D3DXMATRIX GetMatrix(int handle)
	{
		return datas_[handle]->matrix;
	}


	//レイキャスト（レイを飛ばして当たり判定）
	void RayCast(int handle, RayCastData *data)
	{
		D3DXVECTOR3 target = data->start + data->dir;
		D3DXMATRIX matInv;
		D3DXMatrixInverse(&matInv, 0, &datas_[handle]->matrix);
		D3DXVec3TransformCoord(&data->start, &data->start, &matInv);
		D3DXVec3TransformCoord(&target, &target, &matInv);
		data->dir = target - data->start;

		datas_[handle]->pFbx->RayCast(data);
	}

	//テクスチャ再設定
	void SetTexture(std::string fileName, int hModel)
	{
		datas_[hModel]->pFbx->SetTexture(fileName);
	}
}