#include "IGameObject.h"
#include "Collider.h"
#include "Global.h"



IGameObject::IGameObject()
	:IGameObject(nullptr, "")
{
}

IGameObject::IGameObject(IGameObject * parent)
	:IGameObject(parent, "")
{
}

IGameObject::IGameObject(IGameObject * parent, const std::string & name) 
	:pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)), dead_(false),
	pCollider_(nullptr)
{
	D3DXMatrixIdentity(&localMatrix_);
	D3DXMatrixIdentity(&worldMatrix_);
}


IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
}

void IGameObject::Transform()
{
	//移動行列を作成
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
	//回転行列の作成
	//X
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	//Y
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	//Z
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));
	//拡大行列の作成
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);
	//行列を合わせる	拡大 * 回転 * 位置
	localMatrix_ = matS * matRX * matRY * matRZ * matT;
	//ワールド行列
	if (pParent_ != nullptr)
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_;
	}
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	assert(pObj != nullptr);
	//子のリストの末尾に追加
	childList_.push_back(pObj);
}

bool IGameObject::IsExistInChildList(std::string& objectName)
{
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		if ((*it)->GetName() == objectName)
		{
			return true;
		}
	}

	return false;
}


void IGameObject::UpdateSub()
{
	//更新処理
 	Update();
	Transform();

	if (pCollider_ != nullptr)
	{
		//現在のシーン以下のオブジェクトとの当たり判定
		Collision(SceneManager::GetCurrentScene());
	}

	//子の更新
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			//消した次をitに入れる
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
		
	}
}

void IGameObject::DrawSub()
{
	//描画処理
	Draw();

	//子の描画
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	//解放処理
	Release();

	//子の解放
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	dead_ = true;
}

void IGameObject::SetCollider(D3DXVECTOR3 & center, float radius)
{
	pCollider_ = new Collider(this, center, radius);
}

void IGameObject::Collision(IGameObject* targetObject)
{
	//衝突処理
	if (targetObject != this &&
		targetObject->pCollider_ != nullptr && 
		pCollider_->IsHit(targetObject->pCollider_))
	{
		//当たった
		OnCollision(targetObject);
	}

	//対象オブジェクトの子との当たり判定
	for (auto itr = targetObject->childList_.begin(); itr != targetObject->childList_.end(); itr++)
	{
		Collision(*itr);
	}	
}

IGameObject * IGameObject::GetChild(int ID)
{
	auto itr = childList_.begin();
	for (int i = 0; i < ID; i++)
	{
		itr++;
	}
	return *itr;
}

