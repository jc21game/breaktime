#include <assert.h>
#include <d3dx9.h>
#include "Input.h"

namespace Input
{
	//DirectInput本体
	LPDIRECTINPUT8   pDInput = nullptr;

	//キーボード
	//デバイスオブジェクト
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	//現在の各キーの状態
	BYTE keyState[256] = { 0 };
	//前フレームでの各キーの状態
	BYTE prevKeyState[256];

	//マウス
	LPDIRECTINPUTDEVICE8 pMouseDevice; //デバイスオブジェクト
	DIMOUSESTATE mouseState;    //マウスの状態
	DIMOUSESTATE prevMouseState;   //前フレームのマウスの状態

	//ウィンドウハンドル
	HWND hWnd_;

	//初期化
	void Initialize(HWND hWnd)
	{
		//DirectInput本体を作成
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		//エラー処理
		assert(pDInput != nullptr);

		//キーボード
		{
			//デバイスオブジェクトの作成
			pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
			//エラー処理
			assert(pKeyDevice != nullptr);
			//デバイスの種類を指定
			pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
			//強調レベル（他の実行中のアプリに対する優先度）の設定
			pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		}

		//マウス
		{
			pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr);
			assert(pKeyDevice);
			pMouseDevice->SetDataFormat(&c_dfDIMouse);
			pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		}

		//ウィンドウハンドル
		HWND hWnd_;
	}

	//更新
	void Update()
	{
		//キーボード
		memcpy(prevKeyState, keyState, sizeof(keyState));
		//キーボードを見失ったときに探す（仕様）
		pKeyDevice->Acquire();
		//その瞬間の全キーの状態を配列に入れる
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//マウス
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->Acquire();
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);
	}

	//解放
	void Release()
	{
		//解放処理
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}

	///////////////////////////// キーボード情報取得 //////////////////////////////////

	//キーが押されたか
	bool IsKey(int keyCode)
	{
		//16進数とAND演算
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//今キーを押したか
	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない（~はビット反転）
		if ((keyState[keyCode] & ~prevKeyState[keyCode]) & 0x80)
		{
			return true;
		}
		return false;
	}

	//今キーを放したか
	bool IsKeyUp(int keyCode)
	{
		//今は押してなくて、前回は押している
		if ((~keyState[keyCode] & prevKeyState[keyCode]) & 0x80)
		{
			return true;
		}
		return false;
	}

	///////////////////////////// マウス情報取得 //////////////////////////////////

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;
		GetCursorPos(&mousePos);
		ScreenToClient(hWnd_, &mousePos);

		D3DXVECTOR3 result = D3DXVECTOR3(mousePos.x, mousePos.y, 0);
		return result;
	}


	//そのフレームでのマウスの移動量を取得
	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result(mouseState.lX, mouseState.lY, mouseState.lZ);
		return result;
	}

}