#pragma once
#include "IGameObject.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	D3DXVECTOR3 target_;			//焦点位置

public:
	//コンストラクタ
	//引数：parent	親オブジェクト
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//セッター
	void SetTarget(D3DXVECTOR3 target) { target_ = target; }
};