#pragma once
#include <string>
#include "Sprite.h"

namespace Image
{
	//画像データ
	struct ImageData
	{
		std::string filename;		//ファイル名
		Sprite* pSprite;			//ロードした画像データのアドレス
		D3DXMATRIX matrix;			//行列

		//構造体のコンストラクタ(初期化)
		ImageData() :filename(""), pSprite(nullptr)
		{
			//行列の初期化
			D3DXMatrixIdentity(&matrix);
		}
	};

	//画像をロード
	//引数：filename	ファイル名
	int Load(std::string filename);

	//描画
	//引数：handle	モデル番号
	void Draw(int handle);

	//行列を設定
	//引数：handle	モデル番号	matrix	オブジェクトの行列
	void SetMatrix(int handle, D3DXMATRIX matrix);

	//指定した画像を解放
	//引数：handle	解放したいモデル番号
	void Release(int handle);

	//指定した画像のサイズを取得
	D3DXVECTOR2 GetTextureSize(int handle);

	//すべて解放
	void AllRelease();

	//指定した画像のアルファ値をセット
	//引数：handle, 
	void SetColor(int handle, float alpha);
};
