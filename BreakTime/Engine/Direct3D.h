#pragma once
#include <Windows.h>
#include <d3dx9.h>

namespace Direct3D
{
	extern LPDIRECT3D9	pD3d;				//Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9 pDevice;		//Direct3Dデバイスオブジェクト(ゲーム画面)

	//Direct3Dの初期化
	//引数：hWnd	ウィンドウハンドル
	//戻り値：なし
	void Initialize(HWND hWnd);

	//描画開始
	//引数：なし
	//戻り値：なし
	void BeginDraw();

	//描画終了
	//引数：なし
	//戻り値：なし
	void EndDraw();

	//解放
	//引数：なし
	//戻り値：なし
	void Release();
}