#include "Audio.h"
#include "Global.h"

namespace Audio
{
	IXACT3Engine*		pXactEngine;	//XACTエンジン本体
	IXACT3WaveBank*		pWaveBank;		//ウェーブバンクを入れるもの
	IXACT3SoundBank*	pSoundBank;		//サウンドバンクを入れるもの

	void* soundBankData;				//サウンドファイルを入れるもの

	void Initialize()
	{
		pXactEngine = nullptr;
		pWaveBank = nullptr;
		pSoundBank = nullptr;

		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		XACT3CreateEngine(0, &pXactEngine);
		XACT_RUNTIME_PARAMETERS xactParam = { 0 };
		xactParam.lookAheadTime = XACT_ENGINE_LOOKAHEAD_DEFAULT;
		pXactEngine->Initialize(&xactParam);
	}

	void LoadWaveBank(char* waveBank)
	{
		//ファイルを開く
		HANDLE hFile = CreateFile(waveBank, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//開いたファイルのサイズ
		DWORD fileSize = GetFileSize(hFile, NULL);


		//ファイルをマッピングする
		HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, fileSize, NULL);
		void* mapWaveBank;
		mapWaveBank = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);

		//WAVEバンク作成
		pXactEngine->CreateInMemoryWaveBank(mapWaveBank, fileSize, 0, 0, &pWaveBank);

		//ファイルを閉じる
		CloseHandle(hMapFile);
		CloseHandle(hFile);
	}

	void LoadSoundBank(char* soundBank)
	{
		//ファイルを開く
		HANDLE hFile = CreateFile(soundBank, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//ファイルのサイズを調べる
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルの中身をいったん配列に入れる
		
		soundBankData = new BYTE[fileSize];
		DWORD byteRead;
		ReadFile(hFile, soundBankData, fileSize, &byteRead, NULL);

		//サウンドバンク作成
		pXactEngine->CreateSoundBank(soundBankData, fileSize, 0, 0, &pSoundBank);

		//ファイルを閉じる
		CloseHandle(hFile);
	}

	void Play(char* cueName)
	{
 		XACTINDEX cueIndex = pSoundBank->GetCueIndex(cueName);
		pSoundBank->Play(cueIndex, 0, 0, NULL);
	}

	void Stop(char* cueName)
	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(cueName);
		pSoundBank->Stop(cueIndex, XACT_FLAG_CUE_STOP_IMMEDIATE);
	}

	void Release()
	{
		if (pSoundBank != nullptr)
		{
			pSoundBank->Destroy();
		}

		SAFE_DELETE_ARRAY(soundBankData);

		if (pWaveBank != nullptr)
		{
			pWaveBank->Destroy();
		}

		if (pXactEngine != nullptr)
		{
			pXactEngine->ShutDown();
		}

		CoUninitialize();
	}

}
