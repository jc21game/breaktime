#include "Collider.h"



Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, float radius)
{
	owner_ = owner;
	center_ = center;
	radius_ = radius;
}


Collider::~Collider()
{
}

bool Collider::IsHit(Collider* target)
{
	//自分と相手の中心間の距離を算出
	D3DXVECTOR3 v = (center_ + owner_->GetPosition()) - 
		(target->center_ + target->owner_->GetPosition());
	float length = D3DXVec3Length(&v);

	//当たり判定
	if (length <= radius_ + target->radius_)
	{
		return true;
	}

	return false;
}
