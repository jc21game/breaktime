#include "Image.h"
#include <vector>

namespace Image
{
	//ロード済みの画像データ一覧
	std::vector<ImageData*> dataList;

	int Load(std::string filename)
	{
		ImageData* pData = new ImageData;
		pData->filename = filename;			//ファイル名を保存

		//画像データ一覧に同じファイル名がないか探す
		bool isExit = false;
		for (UINT i = 0; i < dataList.size(); i++)
		{
			//見つけたら画像データのアドレスをコピー
			if (dataList[i]->filename == filename)
			{
				pData->pSprite = dataList[i]->pSprite;
				isExit = true;
				break;
			}
		}

		//見つからなかったらロード
		if (isExit == false)
		{
			pData->pSprite = new Sprite;
			pData->pSprite->Load(filename.c_str());
		}

		//リストに追加
		dataList.push_back(pData);

		//要素数-1を返す（末尾の添え字）
		return dataList.size() - 1;
	}

	void Draw(int handle)
	{
		if (handle < 0 || handle >= (int)dataList.size() || dataList[handle] == nullptr)
		{
			//画像データがないから描画できない
			return;
		}
		//描画
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX matrix)
	{
		//指定された番号のdataListに行列を設定
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		//他に同じモデルを使っているものがないか調べる
		bool isExist = false;							//存在していたかどうかのフラグ
		for (UINT i = 0; i < dataList.size(); i++)
		{
			if ((i != handle) && (dataList[i] != nullptr) && (dataList[i]->pSprite == dataList[handle]->pSprite))
			{
				isExist = true;
				break;
			}
		}

		//他が使っていなかったら解放
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pSprite);
		}
		//handleの項目を削除
		SAFE_DELETE(dataList[handle]);
	}

	D3DXVECTOR2 GetTextureSize(int handle)
	{
		return dataList[handle]->pSprite->GetTextureSize();
	}

	void AllRelease()
	{
		for (UINT i = 0; i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr)
			{
				Release(i);
			}

		}
		//リストを削除
		dataList.clear();
	}

	void SetColor(int handle, float alpha)
	{
		if (handle < 0 || handle >= (int)dataList.size() || dataList[handle] == nullptr)
		{
			//画像データがないから色の変更できない
			return;
		}
		dataList[handle]->pSprite->Setcolor(alpha);
	}

}
