#pragma once
#include "Engine/IGameObject.h"

//選択位置
enum SELECT {
	SELECT_CALL,
	SELECT_FOLD,
	SELECT_MAX
};

//勝負するかのUIを管理するクラス
class FoldOrCall : public IGameObject
{
private:
	int hPictBoard_;			//背面画像番号
	int hPictFold_;				//「降りる」画像番号
	int hPictCall_;				//「勝負」画像番号
	bool isDisplay_;			//表示するかどうか
	int select_;				//選択位置
	int drawCount_;				//描画経過カウント

public:
	//コンストラクタ
	FoldOrCall(IGameObject* parent);

	//デストラクタ
	~FoldOrCall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//表示切替
	//引数：isDisplay 表示するかどうか
	void Display(bool isDisplay);

	//カーソルを移動
	//引数：selectPos 選択位置
	void SelectCursor(int selectPos);

	//アクセス関数
	int GetSelect() { return select_; }
};