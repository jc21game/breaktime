#pragma once
#include "Engine/IGameObject.h"
#include "School.h"

#define POSITION_X 210
#define POSITION_Y 150






//チームロゴを管理するクラス
class Teamlogo : public IGameObject
{
private:
	int hPict_;
	float clear_;		//α値更新
	float stride_;		//α値の変化式
	LOGO_STATE  logoState_;

public:
	//コンストラクタ
	Teamlogo(IGameObject* parent);

	//デストラクタ
	~Teamlogo();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//フェードイン強制終了
	//引数　alpha：入れるα値
	void SetAlpha(float alpha) { clear_ = alpha; };

	//透明度getter
	//戻り値　clear_ : 透明度
	float GetAlpha() {return clear_; };
};