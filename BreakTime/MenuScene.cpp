#include <string>
#include "MenuScene.h"
#include "Engine/Image.h"
#include "GameDemo.h"

//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene")
{
	for (int i = 0; i < MENU_MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//初期化
void MenuScene::Initialize()
{
	std::string fileName[]{
	//	"data/text.png",		// テキスト画像
		"data/MenuManual.png"		// 背景画像
	};
	for (int i = 0; i < MENU_MAX; i++)
	{
		hPict_[i] = Image::Load(fileName[i]);
		assert(hPict_ >= 0);
		Image::SetColor(hPict_[i], (float)1);
	}

	CreateGameObject<GameDemo>(this);
}

//更新
void MenuScene::Update()
{
	// ENTERで次のシーンに移動
	if (Input::IsKeyUp(DIK_RETURN))
	{
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void MenuScene::Draw()
{
	for (int i = 0; i < MENU_MAX; i++)
	{
		D3DXMATRIX mat;
		D3DXMatrixTranslation(&mat, (float)0, (float)0, (float)i);
		Image::SetMatrix(hPict_[i], mat);
		Image::Draw(hPict_[i]);
	}
}

//開放
void MenuScene::Release()
{
}