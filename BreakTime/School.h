#pragma once
#include "Engine/IGameObject.h"

#define POSITION_X 240
#define POSITION_Y 150

enum LOGO_STATE
{
	LOGO_STATE_FADE,
	LOGO_STATE_END
};

//学校名を管理するクラス
class School : public IGameObject
{
	int hPict_;
	float clear_;		// α値更新
	float stride_;		// α値の変化式
	LOGO_STATE  logoState_;

public:
	//コンストラクタ
	School(IGameObject* parent);

	//デストラクタ
	~School();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};