#pragma once
#include <time.h>
#include "Engine/IGameObject.h"

#define POSITION_X 150		//x位置
#define POSITION_Y 0		//y位置

#define TIME_DELTA 6		//画像の切り替え間隔(秒)

const float STRIDE = 0.02f;	//透明度の変化量

//画像状態
enum DEMO_STATE
{
	DEMO_STATE_1,
	DEMO_STATE_2,
	DEMO_STATE_3,
	DEMO_STATE_4,
	DEMO_STATE_5,
	DEMO_STATE_6,
	DEMO_STATE_7,
	DEMO_STATE_MAX
};

//ゲームデモ画像を管理するクラス
class GameDemo : public IGameObject
{
	int hPict_[DEMO_STATE_MAX];		//デモ画像番号
	int hPictBoard_;				//背景の画像番号
	float alpha_;					//透明度
	int pictState_;					//現在の画像状態
	time_t currentTime_;			//基準の時間

public:
	//コンストラクタ
	GameDemo(IGameObject* parent);

	//デストラクタ
	~GameDemo();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};