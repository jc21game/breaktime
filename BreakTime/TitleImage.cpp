#include "TitleImage.h"
#include "Engine/Image.h"

//コンストラクタ
TitleImage::TitleImage(IGameObject * parent)
	:IGameObject(parent, "TitleImage"), 
	hPict_(-1), stride_(0.005f), clear_(0)
{
}

//デストラクタ
TitleImage::~TitleImage()
{
}

//初期化
void TitleImage::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/TitleLogo.png");
	assert(hPict_ >= 0);

	position_ = D3DXVECTOR3((float)g.screenWidth / 2, (float)g.screenHeight / 2, 0);
}

//更新
void TitleImage::Update()
{
	if (clear_ <= 1)
	{
		clear_ += stride_;
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		clear_ = 1;
	}

	////スタートボタン生成
	//if (clear_ >= 0.2 && doCreate_ == false)
	//{
	//	CreateGameObject<StartButton>(pParent_);
	//	doCreate_ = true;
	//}


	//指定した画像の透明度をセット
	Image::SetColor(hPict_, clear_);
}

//描画
void TitleImage::Draw()
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, (float)0, (float)0, (float)0);
	Image::SetMatrix(hPict_, mat);
	Image::Draw(hPict_);
}

//開放
void TitleImage::Release()
{
}


