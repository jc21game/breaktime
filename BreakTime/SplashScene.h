#pragma once

#include "Engine/global.h"

enum SPLASH_LOGO 
{
	SPLASH_LOGO_SCHOOL,
	SPLASH_LOGO_TEAM,
	SPLASH_LOGO_MAX
};

class Camera;


//スプラッシュシーンを管理するクラス
class SplashScene : public IGameObject
{
private:
	SPLASH_LOGO currentLogo_;

public:
	//コンストラクタ
	//引数　parent：親オブジェクト
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//次に移る
	//引数　flg：クリックされたら->true、その他->false
	void MoveNext(bool flg);
	
};