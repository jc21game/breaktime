#pragma once
#include "Engine/IGameObject.h"

//テキスト（NPCのセリフ）
enum TEXT {
	//仮
	TEXT_START,
	TEXT_NPC_ROUND_WIN,
	TEXT_NPC_WIN,
	TEXT_PLAYER_ROUND_WIN,
	TEXT_PLAYER_WIN,
	TEXT_MAX

};

//ログを管理するクラス
class Text : public IGameObject
{
	int hPict_;								//枠画像番号
	int hText_[TEXT_MAX];					//テキスト画像番号
	TEXT text_;								//現在表示するテキスト
	std::string fileName_[TEXT_MAX];		//テキストのファイル名
	D3DXVECTOR3 textPos_;					//文の表示位置
	bool isDisplay_;						//表示状態
	int deltaTime_;							//テキスト画像を表示してからの時間(フレーム数)

public:
	//コンストラクタ
	Text(IGameObject* parent);

	//デストラクタ
	~Text();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//テキスト表示
	//引数：type	追加したいテキストに対応したenum
	void DisplayTemplate(TEXT type);

};