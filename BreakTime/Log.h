#pragma once
#include "Engine/IGameObject.h"

enum TEMPLATE {
	//数字
	TEMPLATE_0,								//0
	TEMPLATE_1,								//1
	TEMPLATE_2,								//2
	TEMPLATE_3,								//3
	TEMPLATE_4,								//4
	TEMPLATE_5,								//5

	//接続系
	TEMPLATE_GA,							//が
	TEMPLATE_DE,							//で
	TEMPLATE_VS,							//対

	//主語
	TEMPLATE_NPC,							//ＮＰＣ
	TEMPLATE_PC,							//あなた
	TEMPLATE_HAND_NPC,						//ＮＰＣ：
	TEMPLATE_HAND_PC,						//あなた：

	//文
	TEMPLATE_START,							//ゲームスタート
	TEMPLATE_SHUFFLE,						//山札がシャッフルされました
	TEMPLATE_TRADE,							//枚交換しました
	TEMPLATE_NO_TRADE,						//は交換しませんでした
	TEMPLATE_NO_PAIR,						//はノーペアです
	TEMPLATE_ONE_PAIR,						//ワンペア
	TEMPLATE_TWO_PAIR,						//ツーペア
	TEMPLATE_THREE_CARD,					//スリーカード
	TEMPLATE_STRAIGHT,						//ストレート
	TEMPLATE_FLUSH,							//フラッシュ
	TEMPLATE_FULL_HOUSE,					//フルハウス
	TEMPLATE_FOUR_CARD,						//フォーカード
	TEMPLATE_STRAIGHT_FLUSH,				//ストレートフラッシュ
	TEMPLATE_ROYAL_FLUSH,					//ロイヤルストレートフラッシュ
	TEMPLATE_MAKE_HAND,						//を揃えました
	TEMPLATE_ROUND,							//このラウンドは
	TEMPLATE_GAME,							//このゲームは
	TEMPLATE_WIN,							//の勝ちです
	TEMPLATE_MAX
};

//ログ表示文の情報
struct TemplateData {
	int type;			//表示したい文
	int player;			//プレイヤ
	int number1;		//必要な情報を入れる（交換数 or ラウンドPC勝数）いらないときはNULL
	int number2;		//必要な情報を入れる2（ラウンドNPC勝数）いらないときはNULL
};

//ログを管理するクラス
class Log : public IGameObject
{
	int hPict_;								//画像番号
	int hTemplate_[TEMPLATE_MAX];			//定型文画像番号
	std::string fileName_[TEMPLATE_MAX];	//定型文のファイル名
	D3DXVECTOR3 templatePos_;				//文の表示位置
	std::list<TemplateData*> logList_;		//ログのリスト

public:
	//コンストラクタ
	Log(IGameObject* parent);

	//デストラクタ
	~Log();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//文を追加
	//引数：type	追加したい文に対応したenum
	//		player	対象のプレイヤ（ないならNULL）
	//		number1	必要な情報1、交換カード枚数、プレイヤの勝数（ないならNULL）
	//		number2	必要な情報2、ゲーム結果時のNPCの勝数でのみ使用（それ以外はNULL）
	void AddTemplate(int type, int player, int number1, int number2);

};