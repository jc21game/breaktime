#include <time.h>
#include "StandardAI.h"
#include "Hand.h"
#include "Player.h"

#define CHANGE_COST_MAX 2		//最大交換枚数

StandardAI::StandardAI(Player* parent) : AI(parent, CHANGE_COST_MAX)
{
}


StandardAI::~StandardAI()
{
}

void StandardAI::SelectChangeCard()
{
	Hand* pHand = parent_->GetHand();
	int selectCount = 0;

	//手札のペア情報を取得
	std::list<PAIR>* pPairList;
	pPairList = pHand->GetPairList();
	int pairCount = pPairList->size();

	//一番多いsuitを取得
	int manySuit = pHand->GetManySuit();

	//狙う役に合わせてカードを選択
	switch (targetHand_)
	{
	case POKER_HAND_ONE:
		//最大交換枚数分ループ
		for (int i = 0; i < changeCostMax_; )
		{
			//ランダムに交換
			int changePos = rand() % HAND_MAX;
			//選択状態でない
			if (pHand->GetChild(changePos)->GetPosition().y != 0.3f)
			{
				//選択
				parent_->ChangeSelectState(changePos, true);
				i++;
			}
		}
		break;

	case POKER_HAND_TWO:
	case POKER_HAND_THREE:
		//ペアではないカードをchangecostMax枚交換
		//乱数の種
		srand(time(NULL));

		//最大交換枚数分ループ
		for (int i = 0; i < changeCostMax_; )
		{
			//ランダムに交換
			int changePos = rand() % HAND_MAX;
			//選択状態でない
			if (pHand->GetChild(changePos)->GetPosition().y != 0.3f)
			{
				int num = pHand->GetCardListData(changePos)->number;
				//ペアではないか
				bool isExist = false;
				for (auto it = pPairList->begin(); it != pPairList->end(); it++)
				{
					if ((*it).number == num)
					{
						isExist = true;
					}
				}
				//ペアリストに存在しない数字
				if (!isExist)
				{
					//選択
					parent_->ChangeSelectState(changePos, true);
					i++;
				}
			}
		}
		break;

	case POKER_HAND_STRAIGHT:
	case POKER_HAND_STRAIGHT_FLUSH:
	case POKER_HAND_ROYAL_FLUSH:
		//連続した数字以外を選択
		for (int i = 0; i < HAND_MAX; i++)
		{
			int num = pHand->GetCardListData(i)->number;
			if (num < straightNumMin_ || num > straightNumMax_)
			{
				parent_->ChangeSelectState(i, true);
			}
		}
		break;

	case POKER_HAND_FLUSH:
		//一番多いsuit以外を選択
		for (int i = 0; i < HAND_MAX; i++)
		{
			int suit = pHand->GetCardListData(i)->suit;
			if (suit != manySuit)
			{
				parent_->ChangeSelectState(i, true);
			}
		}
		break;

	case POKER_HAND_FULL_HOUSE:
	case POKER_HAND_FOUR:
		//ペア以外の数字を選択
		//最大交換枚数分ループ
		for (int i = 0; i < HAND_MAX; i++)
		{
			int num = pHand->GetCardListData(i)->number;
			//ペアではないか
			bool isExist = false;
			for (auto it = pPairList->begin(); it != pPairList->end(); it++)
			{
				if ((*it).number == num)
				{
					isExist = true;
					break;
				}
			}
			//ペアリストに存在しない数字
			if (!isExist)
			{
				//選択
				parent_->ChangeSelectState(i, true);
				selectCount++;
			}

			//最大交換枚数以上になったら選択終了
			if (selectCount >= changeCostMax_)
			{
				break;
			}
		}
		break;
	}
}

void StandardAI::PowerToEmotion()
{
	int power = parent_->GetHand()->GetPower();
	power = (power >> 20);

	if (power <= POKER_HAND_TWO)
	{
		emotion_ = EMOTION_WORRY;
	}
	else if (power >= POKER_HAND_FULL_HOUSE)
	{
		emotion_ = EMOTION_FUNNY;
	}
	else
	{
		emotion_ = EMOTION_STANDARD;
	}

}