#include "BackGround.h"
#include "Engine/Image.h"
#include "TitleImage.h"


//コンストラクタ
BackGround::BackGround(IGameObject * parent)
	:IGameObject(parent, "BackGround"), 
	hPict_(-1), stride_(0.005f), clear_(0), isLoad_(false), pTitleImage_(nullptr)
{
}

//デストラクタ
BackGround::~BackGround()
{
}

//初期化
void BackGround::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/TitleBackGround.png");
	assert(hPict_ >= 0);
}

//更新
void BackGround::Update()
{
	clear_ += stride_;

	if (clear_ > 1 && isLoad_ == false )
	{
		pTitleImage_ = CreateGameObject<TitleImage>(this);
		isLoad_ = true;
		stride_ = 0;

	}

 	if (Input::IsKeyDown(DIK_SPACE) && isLoad_ == false)
	{
		clear_ = 1;
		stride_ = 0;

		pTitleImage_ = CreateGameObject<TitleImage>(this);
		isLoad_ = true;

		//TitleImage* pTitleImage;
		//pTitleImage->IsClick();
	}


	//指定した画像の透明度をセット
	Image::SetColor(hPict_, clear_);

}

//描画
void BackGround::Draw()
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, (float)0, (float)0, (float)1);
	Image::SetMatrix(hPict_, mat);
	Image::Draw(hPict_);
}

//開放
void BackGround::Release()
{
}