#include "Log.h"
#include "Engine/Image.h"

#define HEIGHT			470	//画面上端からの高さ
#define WIDTH			30	//画面左端からの幅
#define LINE_SPACING	6	//行間
#define LOG_MAX			12	//ログの最大表示数
#define CHAR_SIZE		12	//文字の大きさ
#define TEMPLATE_POSITION D3DXVECTOR3(10, 5 ,0)	//一番上の文の表示位置
#define PICT_MAX		7	//一文の表示画像最大数


//コンストラクタ
Log::Log(IGameObject * parent)
	:IGameObject(parent, "Log"), hPict_(-1), templatePos_(0, 0, 0)
{
	//画像番号を-1で初期化
	for (int i = 0; i < TEMPLATE_MAX; i++)
	{
		hTemplate_[i] = -1;
	}
}

//デストラクタ
Log::~Log()
{
	//ログのリストを開放
	for (auto it = logList_.begin(); it != logList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}
}

//初期化
void Log::Initialize()
{
	//ログ画像データのロード
	hPict_ = Image::Load("data/log.png");
	assert(hPict_ >= 0);
	Image::SetColor(hPict_, 1);

	//テンプレート文のファイルパスを格納
	fileName_[TEMPLATE_0]				= "0";
	fileName_[TEMPLATE_1]				= "1";
	fileName_[TEMPLATE_2]				= "2";
	fileName_[TEMPLATE_3]				= "3";
	fileName_[TEMPLATE_4]				= "4";
	fileName_[TEMPLATE_5]				= "5";
	fileName_[TEMPLATE_GA]				= "Ga";
	fileName_[TEMPLATE_DE]				= "De";
	fileName_[TEMPLATE_VS]				= "Vs";
	fileName_[TEMPLATE_NPC]				= "NPC";
	fileName_[TEMPLATE_PC]				= "PC";
	fileName_[TEMPLATE_NPC]				= "NPC";
	fileName_[TEMPLATE_HAND_PC]			= "Hand_PC";
	fileName_[TEMPLATE_HAND_NPC]		= "Hand_NPC";
	fileName_[TEMPLATE_START]			= "GameStart";
	fileName_[TEMPLATE_SHUFFLE]			= "Shuffle";
	fileName_[TEMPLATE_TRADE]			= "Trade";
	fileName_[TEMPLATE_NO_TRADE]		= "Trade_No";
	fileName_[TEMPLATE_ONE_PAIR]		= "OnePair";
	fileName_[TEMPLATE_TWO_PAIR]		= "TwoPair";
	fileName_[TEMPLATE_THREE_CARD]		= "ThreeCard";
	fileName_[TEMPLATE_STRAIGHT]		= "Straight";
	fileName_[TEMPLATE_FLUSH]			= "Flush";
	fileName_[TEMPLATE_FULL_HOUSE]		= "FullHouse";
	fileName_[TEMPLATE_FOUR_CARD]		= "FourCard";
	fileName_[TEMPLATE_STRAIGHT_FLUSH]	= "StraightFlush";
	fileName_[TEMPLATE_ROYAL_FLUSH]		= "RoyalFlush";
	fileName_[TEMPLATE_MAKE_HAND]		= "MakeHand";
	fileName_[TEMPLATE_NO_PAIR]			= "NoPair";
	fileName_[TEMPLATE_ROUND]			= "Round";
	fileName_[TEMPLATE_GAME]			= "Game";
	fileName_[TEMPLATE_WIN]				= "Win";


	//テンプレート文の画像データのロード
	for (int i = 0; i < TEMPLATE_MAX; i++)
	{
		hTemplate_[i] = Image::Load("data/Template/" + fileName_[i] + ".png");
		assert(hTemplate_[i] >= 0);
		Image::SetColor(hTemplate_[i], 1);
	}

	//ログ本体の位置を設定
	position_ = D3DXVECTOR3(WIDTH, HEIGHT, 0);
}

//更新
void Log::Update()
{
	//最大数より多ければ古いものを削除
	if (logList_.size() > LOG_MAX)
	{
		auto it = logList_.begin();
		SAFE_DELETE(*it);
		logList_.pop_front();
	}
}

//描画
void Log::Draw()
{
	//ログ本体を描画
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);

	//文を描画
	//文の描画位置を設定
	templatePos_ = position_ + TEMPLATE_POSITION;
	int templateNumbers[PICT_MAX];		//テンプレート文の画像番号
	int chars[PICT_MAX];				//画像の文字数
	int pictCount = 0;					//画像数

	for (auto it = logList_.begin(); it != logList_.end(); it++)
	{
		//位置
		D3DXMATRIX matrix;
		D3DXMatrixTranslation(&matrix, templatePos_.x, templatePos_.y, templatePos_.z);

		switch ((*it)->type)
		{
		//1つの画像
		case TEMPLATE_SHUFFLE:
		case TEMPLATE_START:
			//画像数
			pictCount = 1;

			//各画像番号
			templateNumbers[0] = (*it)->type;

			break;

		//2つの画像の組み合わせ
		case TEMPLATE_ONE_PAIR:
		case TEMPLATE_TWO_PAIR:
		case TEMPLATE_THREE_CARD:
		case TEMPLATE_STRAIGHT:
		case TEMPLATE_FLUSH:
		case TEMPLATE_FULL_HOUSE:
		case TEMPLATE_FOUR_CARD:
		case TEMPLATE_STRAIGHT_FLUSH:
		case TEMPLATE_ROYAL_FLUSH:
		case TEMPLATE_NO_PAIR:
		case TEMPLATE_NO_TRADE:
			//画像数
			pictCount = 2;

			//１つ目の画像の文字数
			if ((*it)->type == TEMPLATE_NO_TRADE)
			{
				chars[0] = 3;
			}
			else
			{
				chars[0] = 4;
			}

			//各画像番号
			templateNumbers[0] = (*it)->player;
			templateNumbers[1] = (*it)->type;

			break;

		//3つの画像の組み合わせ
		case TEMPLATE_ROUND:
			//画像数
			pictCount = 3;

			//各画像の文字数の設定
			chars[0] = 7;
			chars[1] = 3;

			//各画像番号
			templateNumbers[0] = (*it)->type;
			templateNumbers[1] = (*it)->player;
			templateNumbers[2] = TEMPLATE_WIN;

			break;

		//4つの画像の組み合わせ
		case TEMPLATE_TRADE:
			//画像数
			pictCount = 4;

			//各画像の文字数の設定
			chars[0] = 3;
			chars[1] = 1;
			chars[2] = 1;

			//各画像番号
			templateNumbers[0] = (*it)->player;
			templateNumbers[1] = TEMPLATE_GA;
			templateNumbers[2] = (*it)->number1;
			templateNumbers[3] = TEMPLATE_TRADE;

			break;

		//7つの画像の組み合わせ
		case TEMPLATE_GAME:
			////画像数
			//pictCount = 7;

			////各画像の文字数の設定
			//chars[0] = 6;
			//chars[1] = 1;
			//chars[2] = 1;
			//chars[3] = 1;
			//chars[4] = 1;
			//chars[5] = 3;
			//chars[6] = 5;

			////各画像番号
			//templateNumbers[0] = (*it)->type;
			//templateNumbers[1] = (*it)->number1;
			//templateNumbers[2] = TEMPLATE_VS;
			//templateNumbers[3] = (*it)->number2;
			//templateNumbers[4] = TEMPLATE_DE;
			//templateNumbers[5] = (*it)->player;
			//templateNumbers[6] = TEMPLATE_WIN;

			//画像数
			pictCount = 3;

			//各画像の文字数の設定
			chars[0] = 6;
			chars[1] = 3;
			chars[2] = 5;

			//各画像番号
			templateNumbers[0] = (*it)->type;
			templateNumbers[1] = (*it)->player;
			templateNumbers[2] = TEMPLATE_WIN;

			break;
		}

		//各情報をもとに描画
		for (int i = 0; i < pictCount; i++)
		{
			//描画
			Image::SetMatrix(hTemplate_[templateNumbers[i]], matrix);
			Image::Draw(hTemplate_[templateNumbers[i]]);

			//最後の画像でなければ
			if (i != pictCount - 1)
			{
				//位置
				templatePos_.x += CHAR_SIZE * chars[i];
				D3DXMatrixTranslation(&matrix, templatePos_.x, templatePos_.y, templatePos_.z);
			}
			else if (templateNumbers[i] == TEMPLATE_WIN)
			{
				//表示位置更新
				templatePos_.y += CHAR_SIZE + (LINE_SPACING * 2);
				templatePos_.x = position_.x + TEMPLATE_POSITION.x;
			}
			else
			{
				//表示位置更新
				templatePos_.y += CHAR_SIZE + LINE_SPACING;
				templatePos_.x = position_.x + TEMPLATE_POSITION.x;
			}
		}
	}
}

//開放
void Log::Release()
{
	//ログのリストを開放
	for (auto it = logList_.begin(); it != logList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}

	logList_.clear();
}

//文章追加
void Log::AddTemplate(int type, int player, int number1, int number2)
{
	TemplateData* pTemplateData = new TemplateData;
	pTemplateData->type = type;
	pTemplateData->player = player;
	pTemplateData->number1 = number1;
	pTemplateData->number2 = number2;

	//先頭に追加
	logList_.push_back(pTemplateData);

}
