#pragma once
#include "Engine/IGameObject.h"
#include "CardData.h"

//捨て札を管理するクラス
class Discards : public IGameObject
{
	bool isReset_;					//リセットされたか

public:
	//コンストラクタ
	Discards(IGameObject* parent);

	//デストラクタ
	~Discards();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//カードを追加
	//引数 cardData：カード情報
	void AddCard(CardData* cardData);

	//捨て札をリセット
	void Reset();

	//
	void OrderToReset();
};