#pragma once
#include "Engine/IGameObject.h"

//◆◆◆を管理するクラス
class TestCard : public IGameObject
{
	int hModel_[7];			//モデル番号
public:
	//コンストラクタ
	TestCard(IGameObject* parent);

	//デストラクタ
	~TestCard();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};