#pragma once
#include "Player.h"

class FoldOrCall;

class PlayerCharacter :
	public Player
{
private:
	FoldOrCall* pFoldOrCall_;		//「勝負」「降りる」のUI
public:
	PlayerCharacter(IGameObject * parent);
	~PlayerCharacter();
	void InitHandCardsPos() override;
	void InitBetBoardPos() override;
	void Draw() override;
	//山札からカードを引き、手札に加える
	//引数 addPosition:手札の何番目に加えるか 
	void PullCard(int addPosition) override;

	//ポイントをかける
	bool betPoint() override;

	//勝負するか選択する
	bool SelectFoldOrCall() override;
};

