#pragma once

#include <time.h>
#include "Engine/global.h"

//画像数
#define IMAGE_NUMBER 3

class BackGround;
class Start;

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
private:
	const double changeTime_;		//画面切り替えの待ち時間
	time_t start_;					//開始時間
	time_t now_;					//現在時間
	BackGround* pBackGround_;		//背景
	Start* pStart_;					//スタートのUI

	bool isStart_;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};