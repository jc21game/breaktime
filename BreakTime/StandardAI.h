#pragma once
#include "AI.h"

class StandardAI : public AI
{
public:
	StandardAI(Player*);
	~StandardAI();

	// 変更するカードを決める
	void SelectChangeCard() override;

	//手札の強さをもとに感情を決定
	void PowerToEmotion() override;
};

