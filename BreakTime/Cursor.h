#pragma once
#include "Engine/IGameObject.h"

//カーソルを管理するクラス
class Cursor : public IGameObject
{
private:
	int hModel_;		//モデル番号
	bool display_;		//描画するか
	int displayChangeCnt_;	//0~DISPLAY_CNT_MAX表示、~VANISH_CNT_MAX非表示
public:
	//コンストラクタ
	Cursor(IGameObject* parent);

	//デストラクタ
	~Cursor();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};