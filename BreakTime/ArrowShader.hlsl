texture		TEXTURE;		//テクスチャ

//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;			//テクスチャ情報が入っている変数名
	//アンチエイリアスをかけるための設定
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	//左右（上下）の足りない分は両端の色を適用
	AddressU = Clamp;
	AddressV = Clamp;
};

//頂点シェーダーの出力、ピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos		: SV_POSITION;		//位置
	float2 uv		: TEXCOORD0;		//UV座標
};

//頂点シェーダー
//頂点座標（ローカル座標）をスクリーン座標に変換
//引数：pos	頂点座標(ローカル), uv UV座標
//戻り値：頂点のスクリーン座標
VS_OUT VS(float4 pos : POSITION, float2 uv : TEXCOORD0)
{
	VS_OUT outData;
	outData.pos = pos;
	outData.uv = uv;
	return outData;
};

//ピクセルシェーダー
//引数：pos スクリーン座標
//戻り値：その座標の色情報
float4 PS(VS_OUT inData) : COLOR
{
	//色
	//float4 color = tex2D(texSampler, inData.uv);
	float4 color = float4(0, 0, 0, 0);
	//int power = 7;		//範囲
	//int count = 0;
	////周囲マスの平均値をとる
	//for (int x = -power; x <= power; x++)
	//{
	//	for (int y = -power; y <= power; y++)
	//	{
	//		color += tex2D(texSampler, float2(inData.uv.x + (1.0 / 10) * x, inData.uv.y + (1.0 / 10) * y));
	//		count++;
	//	}
	//}
	//color /= count;
	return color;
}


technique
{
	//どの関数を使うか設定(バックミラーなどを作る場合は2回描画するためpassが2つ必要)
	pass
	{
		//頂点シェーダー		vs_3_0 でコンパイル(シェーダーは実行時にコンパイルされる)
		VertexShader = compile vs_3_0 VS();
		//ピクセルシェーダー
		PixelShader = compile ps_3_0 PS();
	}
}