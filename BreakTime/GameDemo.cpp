#include "GameDemo.h"
#include "Engine/Image.h"
#include "SplashScene.h"

//コンストラクタ
GameDemo::GameDemo(IGameObject * parent)
	:IGameObject(parent, "GameDemo"), hPictBoard_(-1),
	alpha_(1.0f), pictState_(DEMO_STATE_1)
{
	//初期化
	for (int i = 0; i < DEMO_STATE_MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
GameDemo::~GameDemo()
{
}

//初期化
void GameDemo::Initialize()
{
	//背景画像ロード
	hPictBoard_ = Image::Load("data/MenuRuleBack.png");
	assert(hPictBoard_ >= 0);

	//背景の透明度初期化
	Image::SetColor(hPictBoard_, 1.0f);

	//パスを格納
	std::string path[DEMO_STATE_MAX]{
		"data/MenuRule1.png",
		"data/MenuRule2.png",
		"data/MenuRule3.png",
		"data/MenuRule4.png",
		"data/MenuRule5.png",
		"data/MenuRule6.png",
		"data/MenuRule7.png"
	};

	//デモ画像をロード
	for (int i = 0; i < DEMO_STATE_MAX; i++)
	{
		hPict_[i] = Image::Load(path[i]);
		assert(hPict_[i] >= 0);
	}

	//現在時間を基準として保存
	currentTime_ = time(nullptr);

}

//更新
void GameDemo::Update()
{
	//現在時間を取得
	time_t nowTime = time(nullptr);

	//基準時間から一定時間以上過ぎている
	if ((nowTime - currentTime_) >= TIME_DELTA)
	{
		//フェードアウト
		alpha_ -= STRIDE;
	}
	//フェードインが完了していない
	else if (alpha_ < 1.0f)
	{
		//フェードイン
		alpha_ += STRIDE;
	}

	//フェードアウトが完了
	if (alpha_ <= 0)
	{
		//次の画像へ
		pictState_++;

		//最後の画像だった
		if (pictState_ >= DEMO_STATE_MAX)
		{
			//最初の画像に
			pictState_ = DEMO_STATE_1;
		}

		//基準時間を更新
		currentTime_ = nowTime;
	}

	//透明度を更新
	Image::SetColor(hPict_[pictState_], alpha_);
}

//描画
void GameDemo::Draw()
{
	//位置を指定
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, (float)POSITION_X, (float)POSITION_Y, (float)0);

	//描画
	//背景
	Image::SetMatrix(hPictBoard_, mat);
	Image::Draw(hPictBoard_);
	//デモ
	Image::SetMatrix(hPict_[pictState_], mat);
	Image::Draw(hPict_[pictState_]);
}

//開放
void GameDemo::Release()
{
}