#include "Text.h"
#include "Engine/Image.h"
#include "BetBoard.h"

#define FRAME_HEIGHT	50			//画面上端からの高さ
#define FRAME_WIDTH		700			//画面左端からの幅
#define DISPLAY_TIME	190			//テキストの表示時間(フレーム数)

//コンストラクタ
Text::Text(IGameObject * parent)
	:IGameObject(parent, "Text"), hPict_(-1), text_(TEXT_START), textPos_(0, 0, 0), isDisplay_(false), deltaTime_(0)
{
	//画像番号を-1で初期化
	for (int i = 0; i < TEXT_MAX; i++)
	{
		hText_[i] = -1;
	}
}

//デストラクタ
Text::~Text()
{
}

//初期化
void Text::Initialize()
{
	//枠画像データのロード
	hPict_ = Image::Load("data/dialogue/Message_Frame_ellipse.png");
	assert(hPict_ >= 0);
	

	//テキストのファイルパスを格納（格納しているパスは仮）
	fileName_[TEXT_START] = "BattleStart";
	fileName_[TEXT_NPC_ROUND_WIN] = "NpcRoundWin";
	fileName_[TEXT_NPC_WIN] = "NpcWin";
	fileName_[TEXT_PLAYER_ROUND_WIN] = "PlayerRoundWin";
	fileName_[TEXT_PLAYER_WIN] = "PlayerWin";
	

	//テキスト画像データのロード
	for (int i = 0; i < TEXT_MAX; i++)
	{
		hText_[i] = Image::Load("data/dialogue/" + fileName_[i] + ".png");
		assert(hText_[i] >= 0);
	}

	//ログ本体の位置を設定
	position_ = D3DXVECTOR3(FRAME_WIDTH, FRAME_HEIGHT, 0);
}

//更新
void Text::Update()
{
	//画像表示時間用カウント
	deltaTime_++;

	//画像表示後一定時間経過
	if (deltaTime_ > DISPLAY_TIME)
	{
		//非表示
		isDisplay_ = false;
	}
	
}

//描画
void Text::Draw()
{
	//ログ本体を描画
	Image::SetColor(hPict_, isDisplay_);
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);

	//テキストを描画
	Image::SetColor(hText_[text_], isDisplay_);
	Image::SetMatrix(hText_[text_], worldMatrix_);
	Image::Draw(hText_[text_]);
}

//開放
void Text::Release()
{
}

//テキスト表示
void Text::DisplayTemplate(TEXT type)
{
	//表示する画像を変更
	text_ = type;
	//表示
	isDisplay_ = true;
	//表示時間を初期化
	deltaTime_ = 0;
}

