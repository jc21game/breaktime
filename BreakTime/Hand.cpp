#include <algorithm>
#include "Hand.h"
#include "Card.h"
#include "PlayScene.h"

//コンストラクタ
Hand::Hand(IGameObject * parent)
	:IGameObject(parent, "Hand"), pPullingCard_(nullptr), initflag_(false)
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		cardList_[i] = nullptr;
	}

	//ペアリスト生成
	pPairList_ = new std::list<PAIR>;
}

//デストラクタ
Hand::~Hand()
{
	//開放
	SAFE_DELETE(pPairList_);
}

//初期化
void Hand::Initialize()
{
	//手札の枚数分カードを生成
	for (int i = 0; i < HAND_MAX; i++)
	{
		Card* pCard = CreateGameObject<Card>(this);
	}
}

//更新
void Hand::Update()
{
	//保存してある情報をもとにアニメーションを順番に再生
	if (pPullingCard_ == nullptr && pPullList_.size() != 0)
	{
		pPullingCard_ = pPullList_.front();

		//ドローアニメーションを再生
		switch (pPullingCard_->charcter)
		{
		case CHARACTER_NPC:
			NPCPullAnimation(pPullingCard_->pCard, pPullingCard_->addPosition);
			break;

		case CHARACTER_PC:
			PCPullAnimation(pPullingCard_->pCard, pPullingCard_->addPosition);
			break;
		}
	}
	//最後の一枚のアニメーション再生中
	else if(pPullList_.size() == 1 && pPullingCard_ != nullptr)
	{
		//アニメーションが終了したら削除
		if (pPullingCard_->pCard->IsEndPullAnimation())
		{
			//リストから削除
			pPullList_.pop();

			//開放
			delete(pPullingCard_);
			pPullingCard_ = nullptr;
		}
	}
	//アニメーション再生中
	else if(pPullingCard_ != nullptr)
	{
		//アニメーションが終了したら削除
		//if (pPullingCard_->pCard->IsEndPullAnimation())
		if( (pPullingCard_->pCard->GetAnimationFrame() == 15) || (pPullingCard_->pCard->GetAnimationFrame() == 45) )
		{
			//リストから削除
			pPullList_.pop();

			//開放
			delete(pPullingCard_);
			pPullingCard_ = nullptr;
		}
	}

	
}

//描画
void Hand::Draw()
{
}

//開放
void Hand::Release()
{
}

//手札にカードを追加
void Hand::AddCard(CardData *cardData, int addPosition)
{
	//カード情報をリストに追加
	cardList_[addPosition] = cardData;
	
	//指定位置を選択
	auto it = childList_.begin();
	for (int i = 0; i < addPosition; i++)
	{
		it++;
	}
	//そのカードの情報を設定
	((Card*)*it)->SetCardData(cardData);

	//カードのモデルデータをロード
	((Card*)*it)->LoadModelData();

	//カードを見えるようにする
	//((Card*)*it)->SetDisplay(true);
}

//山札からカードを引く
void Hand::PullCard(CardData * cardData, int addPosition, CHARACTER character)
{
	//カード追加
	AddCard(cardData, addPosition);

	//指定位置を選択
	auto it = childList_.begin();
	for (int i = 0; i < addPosition; i++)
	{
		it++;
	}

	//アニメーション再生用の情報を保存
	AnimationData* animationData = new AnimationData;
	animationData->addPosition = addPosition;
	animationData->charcter = character;
	animationData->pCard = (Card*)*it;
	pPullList_.push(animationData);
}

//手札から任意のカードを取り除く
CardData* Hand::RemoveCard(int removePosition)
{
	//指定位置を選択
	auto it = childList_.begin();
	for (int i = 0; i < removePosition; i++)
	{
		it++;
	}

	//クローンを作成
	Card* pDiscard = ((Card*)(*it))->Clone( (IGameObject*)(((PlayScene*)GetParent()->GetParent())->GetDiscards()) );
	pDiscard->Discard(D3DXVECTOR3(0, 0, -1));

	//そのカードを透明化
	((Card*)*it)->SetDisplay(false);

	//リストから削除
	cardList_[removePosition] = nullptr;

	//透明化したカードの情報を返す
	return ((Card*)*it)->GetCardData();
}

//カードを反転
void Hand::TurnCards()
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		D3DXVECTOR3 rotate = GetChild(i)->GetRotate();

		GetChild(i)->SetRotate(D3DXVECTOR3(0, rotate.y + 180, 0));
	}
}

//NPCのドローアニメーション再生
void Hand::NPCPullAnimation(Card* pCard, int addPosition)
{
	//ドローアニメーションを再生
	pCard->StartNPCPullAnimation(D3DXVECTOR3(3, 0, 0), D3DXVECTOR3(-2.4f + (1.2f * addPosition), 0, 1));
}

//PCのドローアニメーション再生
void Hand::PCPullAnimation(Card* pCard, int addPosition)
{
	//ドローアニメーションを再生
	pCard->StartPCPullAnimation(D3DXVECTOR3(3, 0, 0), D3DXVECTOR3(-2.4f + (1.2f * addPosition), 0, -5));
}

//任意の位置のカードを非表示にする
void Hand::HideCard(int cardPosition)
{
	//指定位置を選択
	auto it = childList_.begin();
	for (int i = 0; i < cardPosition; i++)
	{
		it++;
	}

	((Card*)*it)->SetDisplay(false);
}

//デバッグ用カードリストセット
void Hand::SetCardListDebug(CardData * data)
{
	for (int i = 0; i < HAND_MAX; i++)
	{
		AddCard(&data[i], i);
	}
}

void Hand::GatherCards()
{
	auto it = childList_.begin();

	//初回だけ通る
	if (!initflag_)
	{
		for (int i = 0; i < childList_.size(); i++)
		{
			//もとに戻る位置をセット
			((Card*)*it)->SetRemovePosition((*it)->GetPosition());

			//真ん中のカードの位置を集合位置に
			if (i == MIDDLE_CARD)
			{
				//真ん中のカードの位置取得
				gatherPosition_ = (*it)->GetPosition();
			}
			it++;
		}
		initflag_ = true;

		//イテレータの初期化(次の処理に移るため)
		it = childList_.begin();
	}

	//カードの移動
	for (int num = 0; num < childList_.size(); num++)
	{
		//カードの集合
		((Card*)*it)->GatherMoveCards(gatherPosition_, num);
		it++;
	}
}

void Hand::ReverseCards()
{
	auto it = childList_.begin();

	for (int num = 0; num < childList_.size(); num++)
	{
		//カードの集合
		((Card*)*it)->RemoveCards(gatherPosition_, num);
		it++;
	}
}

bool Hand::IsGather()
{
	auto it = childList_.begin();
	int flagCount = 0;

	for (int i = 0; i < childList_.size(); i++)
	{
		if (((Card*)*it)->GetPosition() == gatherPosition_)
		{
			flagCount++;
		}
		it++;
	}

	//5枚すべて中央に集まったか
	if (flagCount == HAND_MAX)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Hand::IsRemove()
{
	auto it = childList_.begin();
	int flagCount = 0;

	for (int i = 0; i < childList_.size(); i++)
	{
		if (((Card*)*it)->GetPosition() == ((Card*)*it)->GetRemovePosition())
		{
			flagCount++;
		}
		it++;
	}

	//5枚すべて解散したか
	if (flagCount == HAND_MAX)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Hand::SetPokerHand()
{
	//各判定用フラグ
	bool isPair = false;
	bool isStraight = false;
	bool isFlush = false;

	std::vector<CardData*> hand;		//手札(数字の昇順)
	std::vector<CardData*> handResult;	//手札(役の強さ順)

	//手札をコピー
	for (int i = 0; i < HAND_MAX; i++)
	{
		hand.push_back(GetCardListData(i));
	}

	//役の確定
	//フラッシュ判定
	SUIT suit = hand.at(0)->suit;
	for (int i = 1; i < HAND_MAX; i++)
	{
		if (suit != hand.at(i)->suit)
		{
			isFlush = false;
			break;
		}
		else if (i == (HAND_MAX - 1))
		{
			isFlush = true;
		}

	}

	//判定用に並べ替え（昇順）
	std::sort(hand.begin(), hand.end(), [](const CardData* a, CardData* b) {
		return a->number < b->number;
	});

	//ストレート判定
	int number = hand.at(0)->number;
	for (int i = 1; i < HAND_MAX; i++)
	{
		if (number == hand.at(i)->number - 1)
		{
			number = hand.at(i)->number;
		}
		else
		{
			isStraight = false;
			break;
		}

		if (i == (HAND_MAX - 1))
		{
			isStraight = true;
		}
	}

	//ペア判定
	//手札からペアリストを作成
	CreatePairList();
	if (pPairList_->size() > 0)
	{
		isPair = true;
	}
	else
	{
		isPair = false;
	}

	//手札の強さを算出
	//ストレートフラッシュ
	if (isStraight && isFlush)
	{
		//ロイヤルフラッシュ
		if (hand.at(0)->number == 9)
		{
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				handResult.push_back(*it);
			}
			pokerHand_ = POKER_HAND_ROYAL_FLUSH;
		}
		else
		{
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				handResult.push_back(*it);
			}
			pokerHand_ = POKER_HAND_STRAIGHT_FLUSH;
		}
	}
	//ペア
	else if (isPair)
	{
		int pairMax = 0;
		//最大枚数を求める
		for (auto it = pPairList_->begin(); it != pPairList_->end(); it++)
		{
			if (pairMax < (*it).count)
			{
				pairMax = (*it).count;
			}
		}

		//フォーカード
		if (pairMax == 4)
		{
			//その他のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number != pPairList_->begin()->number)
				{
					handResult.push_back(*it);
				}
			}
			//役のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == pPairList_->begin()->number)
				{
					handResult.push_back(*it);
				}
			}
			pokerHand_ = POKER_HAND_FOUR;
		}
		//フルハウス
		else if (pairMax == 3 && pPairList_->size() == 2)
		{
			int threeNumber = 0;			//スリーカードの数
			int twoNumber = 0;			//ツーペアの数

			//ペアの数字を取り出す
			for (auto it = pPairList_->begin(); it != pPairList_->end(); it++)
			{
				if ((*it).count == 3)
				{
					threeNumber = (*it).number;
				}
				else if ((*it).count == 2)
				{
					twoNumber = (*it).number;
				}
			}

			//並べ替え
			//その他のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if (((*it)->number != threeNumber) && ((*it)->number != twoNumber))
				{
					handResult.push_back(*it);
				}
			}
			//ツーペアのカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == twoNumber)
				{
					handResult.push_back(*it);
				}
			}
			//スリーカードのカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == threeNumber)
				{
					handResult.push_back(*it);
				}
			}
			pokerHand_ = POKER_HAND_FULL_HOUSE;
		}
		//スリーカード
		else if (pairMax == 3)
		{
			//並べ替え
			//その他のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number != pPairList_->begin()->number)
				{
					handResult.push_back(*it);
				}
			}
			//役のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == pPairList_->begin()->number)
				{
					handResult.push_back(*it);
				}
			}
			pokerHand_ = POKER_HAND_THREE;
		}
		//ツーペア
		else if (pairMax == 2 && pPairList_->size() == 2)
		{
			int bigNumber = 0;			//ペアの大きいほうの数
			int littleNumber = 0;		//ペアの小さいほうの数

			//ペアの数字を取り出す
			auto it = pPairList_->begin();
			bigNumber = (*it).number;
			it++;
			if (bigNumber < (*it).number)
			{
				littleNumber = bigNumber;
				bigNumber = (*it).number;
			}
			else
			{
				littleNumber = (*it).number;
			}

			//並べ替え
			//その他のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if (((*it)->number != bigNumber) && ((*it)->number != littleNumber))
				{
					handResult.push_back(*it);
				}
			}
			//小さいほうのペアのカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == littleNumber)
				{
					handResult.push_back(*it);
				}
			}
			//大きいほうのペアのカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == bigNumber)
				{
					handResult.push_back(*it);
				}
			}
			pokerHand_ = POKER_HAND_TWO;
		}
		//ワンペア
		else if (pairMax == 2 && pPairList_->size() == 1)
		{
			//並べ替え
			//その他のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number != pPairList_->begin()->number)
				{
					handResult.push_back(*it);
				}
			}
			//役のカード
			for (auto it = hand.begin(); it != hand.end(); it++)
			{
				if ((*it)->number == pPairList_->begin()->number)
				{
					handResult.push_back(*it);
				}
			}
			pokerHand_ = POKER_HAND_ONE;
		}
	}
	//ストレート
	else if (isStraight)
	{
		for (auto it = hand.begin(); it != hand.end(); it++)
		{
			handResult.push_back(*it);
		}
		pokerHand_ = POKER_HAND_STRAIGHT;
	}
	//フラッシュ
	else if (isFlush)
	{
		for (auto it = hand.begin(); it != hand.end(); it++)
		{
			handResult.push_back(*it);
		}
		pokerHand_ = POKER_HAND_FLUSH;
	}
	//ハイカード
	else
	{
		for (auto it = hand.begin(); it != hand.end(); it++)
		{
			handResult.push_back(*it);
		}
		pokerHand_ = POKER_HAND_HIGH;
	}

	//手札の強さを求める
	CalculatePower(&handResult);

	
}

void Hand::CalculatePower(std::vector<CardData*>* pHand)
{
	power_ = 0;
	//役
	power_ += (pokerHand_ << 20);
	//役のカード
	for (int i = HAND_MAX - 1; i >= 0; i--)
	{
		power_ += pHand->at(i)->number << (4 * i);
	}
}

void Hand::SortHand()
{
	std::vector<CardData*> hand;		//手札(数字の昇順)
	std::vector<CardData*> handResult;	//手札(役の強さ順)

	//手札をコピー
	for (int i = 0; i < HAND_MAX; i++)
	{
		hand.push_back(GetCardListData(i));
	}

	//並べ替え（昇順）
	std::sort(hand.begin(), hand.end(), [](const CardData* a, CardData* b) {
		return a->number < b->number;
	});

	auto handItr = hand.begin();
	auto pairItr = pPairList_->begin();
	//フルハウス用
	int threeNumber = 0;		//スリーカードの数
	int twoNumber = 0;			//ツーペアの数
	//ツーペア用
	int bigNumber = 0;			//ペアの大きいほうの数
	int littleNumber = 0;		//ペアの小さいほうの数

	switch (pokerHand_)
	{
	case POKER_HAND_ROYAL_FLUSH:
	case POKER_HAND_STRAIGHT_FLUSH:
	case POKER_HAND_FLUSH:
	case POKER_HAND_STRAIGHT:
	case POKER_HAND_HIGH:
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			handResult.push_back(*handItr);
		}
		break;

	case POKER_HAND_FOUR:
		//その他のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number != pPairList_->begin()->number)
			{
				handResult.push_back(*handItr);
			}
		}
		//役のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == pPairList_->begin()->number)
			{
				handResult.push_back(*handItr);
			}
		}
		break;

	case POKER_HAND_FULL_HOUSE:
		

		//ペアの数字を取り出す
		for (pairItr = pPairList_->begin(); pairItr != pPairList_->end(); pairItr++)
		{
			if ((*pairItr).count == 3)
			{
				threeNumber = (*pairItr).number;
			}
			else if ((*pairItr).count == 2)
			{
				twoNumber = (*pairItr).number;
			}
		}

		//並べ替え
		//その他のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if (((*handItr)->number != threeNumber) && ((*handItr)->number != twoNumber))
			{
				handResult.push_back(*handItr);
			}
		}
		//ツーペアのカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == twoNumber)
			{
				handResult.push_back(*handItr);
			}
		}
		//スリーカードのカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == threeNumber)
			{
				handResult.push_back(*handItr);
			}
		}
		break;

	case POKER_HAND_THREE:
		//並べ替え
		//その他のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number != pPairList_->begin()->number)
			{
				handResult.push_back(*handItr);
			}
		}
		//役のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == pPairList_->begin()->number)
			{
				handResult.push_back(*handItr);
			}
		}
		break;

	case POKER_HAND_TWO:
		

		//ペアの数字を取り出す
		pairItr = pPairList_->begin();
		bigNumber = (*pairItr).number;
		pairItr++;
		if (bigNumber < (*pairItr).number)
		{
			littleNumber = bigNumber;
			bigNumber = (*pairItr).number;
		}
		else
		{
			littleNumber = (*pairItr).number;
		}

		//並べ替え
		//その他のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if (((*handItr)->number != bigNumber) && ((*handItr)->number != littleNumber))
			{
				handResult.push_back(*handItr);
			}
		}
		//小さいほうのペアのカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == littleNumber)
			{
				handResult.push_back(*handItr);
			}
		}
		//大きいほうのペアのカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == bigNumber)
			{
				handResult.push_back(*handItr);
			}
		}
		break;

	case POKER_HAND_ONE:
		//並べ替え
		//その他のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number != pPairList_->begin()->number)
			{
				handResult.push_back(*handItr);
			}
		}
		//役のカード
		for (handItr = hand.begin(); handItr != hand.end(); handItr++)
		{
			if ((*handItr)->number == pPairList_->begin()->number)
			{
				handResult.push_back(*handItr);
			}
		}
		break;
	}

	//手札を渡された手札の順に並び替える
	for (int i = 0; i < HAND_MAX; i++)
	{
		AddCard(handResult.at(i), i);
	}
}

int Hand::GetManySuitCnt()
{
	// 手札のSuitがそれぞれ何枚か
	int SuitCnt[SUIT_MAX] = { 0 };
	// 一番多いSuit
	SUIT ManySuit = SUIT_SPADE;
	for (int i = 0; i < HAND_MAX; i++)
	{
		SuitCnt[cardList_[i]->suit]++;
		// 今カウントしたカードの柄が一番多い柄でない、かつ最も多いスートの数より多い数あるならば
		if (cardList_[i]->suit != ManySuit && SuitCnt[cardList_[i]->suit] > SuitCnt[ManySuit])
		{
			// 今カウントしたカードの柄を最も多い柄にする
			ManySuit = cardList_[i]->suit;
		}
	}

	return SuitCnt[ManySuit];
}

int Hand::GetManySuit()
{
	// 手札のSuitがそれぞれ何枚か
	int SuitCnt[SUIT_MAX] = { 0 };
	// 一番多いSuit
	SUIT ManySuit = SUIT_SPADE;
	for (int i = 0; i < HAND_MAX; i++)
	{
		SuitCnt[cardList_[i]->suit]++;
		// 今カウントしたカードの柄が一番多い柄でない、かつ最も多いスートの数より多い数あるならば
		if (cardList_[i]->suit != ManySuit && SuitCnt[cardList_[i]->suit] > SuitCnt[ManySuit])
		{
			// 今カウントしたカードの柄を最も多い柄にする
			ManySuit = cardList_[i]->suit;
		}
	}

	return ManySuit;
}

int Hand::GetMaxCardNum()
{
	int maxNum = cardList_[0]->number;
	for (int i = 1; i < HAND_MAX; i++)
	{
		if (cardList_[i]->number > maxNum)
		{
			maxNum = cardList_[i]->number;
		}
	}

	return maxNum;
}

std::vector<int> Hand::GetNumVec()
{
	std::vector<int> numData;
	for (int i = 0; i < HAND_MAX; i++)
	{
		numData.push_back(cardList_[i]->number);
	}

	return numData;
}

void Hand::CreatePairList()
{
	//初期化
	pPairList_->clear();

	//手札をコピー
	std::vector<CardData*> hand;		//手札(数字の昇順)
	for (int i = 0; i < HAND_MAX; i++)
	{
		hand.push_back(GetCardListData(i));
	}

	//昇順に並べ替え
	std::sort(hand.begin(), hand.end(), [](const CardData* a, CardData* b) {
		return a->number < b->number;
	});

	//ペア判定
	int count = 1;
	int number = hand.at(0)->number;
	for (int i = 1; i < HAND_MAX; i++)
	{
		if (number == hand.at(i)->number)
		{
			count++;
		}
		else
		{
			if (count > 1)
			{
				pPairList_->push_back(PAIR(number, count));
			}
			number = hand.at(i)->number;
			count = 1;
		}
	}
	if (count > 1)
	{
		pPairList_->push_back(PAIR(number, count));
	}
}

POKER_HAND Hand::GetPokerHand()
{
	return pokerHand_;
}

