#include "School.h"
#include "Engine/Image.h"
#include "SplashScene.h"

//コンストラクタ
School::School(IGameObject * parent)
	:IGameObject(parent, "School"),
	clear_(0), stride_(0.01f), logoState_(LOGO_STATE_FADE)
{
}

//デストラクタ
School::~School()
{
}

//初期化
void School::Initialize()
{
	//学校名
	hPict_ = Image::Load("data/TouhokuDenshi2.png");
	assert(hPict_ >= 0);
}

//更新
void School::Update()
{
	if (1 < clear_)
	{
		logoState_ = LOGO_STATE_END;
		if (2 < clear_)
		{
			stride_ *= (-1);
		}
	}
	clear_ += stride_;
	if (clear_ < 0)
	{
		KillMe();
	}

	if (Input::IsKeyDown(DIK_SPACE))
	{
		switch (logoState_)
		{
		case LOGO_STATE_FADE:
			clear_ = 1.0f;
			break;
		case LOGO_STATE_END:
			((SplashScene*)pParent_)->MoveNext(true);
			KillMe();
			break;
		default:
			break;
		}
	}

	Image::SetColor(hPict_, clear_);
}

//描画
void School::Draw()
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, (float)POSITION_X, (float)POSITION_Y, (float)0);
	
	Image::SetMatrix(hPict_, mat);
	Image::Draw(hPict_);
}

//開放
void School::Release()
{
}