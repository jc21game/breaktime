#pragma once
#include "Player.h"
#include "Emotion.h"

class AI;

class NonPlayerCharacter :
	public Player
{
private:
	AI* pAi_;						//AI
	int hModel_;					//モデル番号
	int hPicts_[EMOTION_MAX];		//画像番号

public:
	NonPlayerCharacter(IGameObject * parent);
	~NonPlayerCharacter();
	void InitHandCardsPos() override;
	void InitBetBoardPos() override;
	void Draw() override;

	//山札からカードを引き、手札に加える
	//引数 addPosition:手札の何番目に加えるか 
	void PullCard(int addPosition) override;

	//狙う役を決める
	void SelectTargetHand();

	//交換するカードを選択する
	void SelectChangeCard();

	//ポイントをかける
	bool betPoint() override;

	//勝負するか決める
	//戻り値：選択が終了したか
	bool SelectFoldOrCall() override;

	//感情を変化
	//引数：emotion 感情
	void SetEmotion(EMOTION emotion);

};

