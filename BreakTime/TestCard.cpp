#include "TestCard.h"
#include "Engine/Model.h"

//コンストラクタ
TestCard::TestCard(IGameObject * parent)
	:IGameObject(parent, "TestCard")
{
	for (int i = 0; i < 7; i++)
	{
		hModel_[i] = -1;
	}
}

//デストラクタ
TestCard::~TestCard()
{
}

//初期化
void TestCard::Initialize()
{
	//モデルデータのロード
	for (int i = 0; i < 6; i++)
	{
		hModel_[i] = Model::Load("data/card.fbx");
		assert(hModel_[i] >= 0);
	}

	hModel_[6] = Model::Load("data/deck.fbx");
	assert(hModel_[6] >= 0);
}

//更新
void TestCard::Update()
{

}

//描画
void TestCard::Draw()
{
	//改良の余地あり
	D3DXMATRIX mat, mat2, mat3, mat4, mat5, matDeck, matDiscard;
	//							 ↓ここの値+1.2ずつ増加。カードの幅1、カードの感覚0.2
	D3DXMatrixTranslation(&mat , -2.9, 0, -5);		//一番左
	D3DXMatrixTranslation(&mat2, -1.7, 0, -5);	
	D3DXMatrixTranslation(&mat3, -0.5, 0, -5);		//カードの中央(テーブルの中心)
	D3DXMatrixTranslation(&mat4,  0.7, 0, -5);	
	D3DXMatrixTranslation(&mat5,  1.9, 0, -5);		//一番右

	D3DXMatrixTranslation(&matDiscard, -3, 0, -2);	//捨て札(カードモデル使用) カードは倒れていない。
	D3DXMatrixTranslation(&matDeck, 3, 0, 0);		//山札

	Model::SetMatrix(hModel_[0], mat);
	Model::Draw(hModel_[0]);

	Model::SetMatrix(hModel_[1], mat2);
	Model::Draw(hModel_[1]);

	Model::SetMatrix(hModel_[2], mat3);
	Model::Draw(hModel_[2]);

	Model::SetMatrix(hModel_[3], mat4);
	Model::Draw(hModel_[3]);

	Model::SetMatrix(hModel_[4], mat5);
	Model::Draw(hModel_[4]);

	Model::SetMatrix(hModel_[5], matDiscard);
	Model::Draw(hModel_[5]);

	Model::SetMatrix(hModel_[6], matDeck);
	Model::Draw(hModel_[6]);
}

//開放
void TestCard::Release()
{
}