#pragma once
#include "Engine/IGameObject.h"
#include "CardData.h"
#define DECK_MAX 52

//◆◆◆を管理するクラス
class Deck : public IGameObject
{
	int hModel_;
	int aryPullRange_;
	CardData* cardDataAry_;
public:
	//コンストラクタ
	Deck(IGameObject* parent);

	//デストラクタ
	~Deck();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//カードをランダムに一枚引く関数
	//引数 なし
	//戻り値 取得したカードデータのアドレス
	CardData* PullCard();
};