#pragma once
#include "Engine/IGameObject.h"
#include "CardData.h"


#define MIDDLE_CARD 2			//手札の真ん中のカード(集まる場所の基準)

enum CARD_STATE {
	CARD_STATE_PULL,
	CARD_STATE_GATHER,
	CARD_STATE_REMOVE,
	CARD_STATE_DISCARD,
	CARD_STATE_WAIT,
	CARD_STATE_RESET,
	CARD_STATE_STANDARD,
	CARD_STATE_MAX
};

//カードを管理するクラス
class Card : public IGameObject
{
private:
	int hModel_;
	CardData data_;
	bool display_;		//表示されてるかどうか
	bool selected_;
	D3DXVECTOR3 move_;	//移動量
	int moveFrame_;
	int nowFrame_;

	D3DXVECTOR3 removePosition_;	//元に戻る位置
	bool isClone_;
	int random_;
	int state_;
	int nextState_;

	float rotateRateY_;
	bool isInitState_;

public:
	//コンストラクタ
	Card(IGameObject* parent);

	//デストラクタ
	~Card();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//モデルデータの読み込み
	void LoadModelData();

	//クローン生成
	Card* Clone(IGameObject* pParent);

	//NPCドローアニメーションを再生
	void StartNPCPullAnimation(D3DXVECTOR3 startPosition, D3DXVECTOR3 endPosition);

	//PCドローアニメーションを再生
	void StartPCPullAnimation(D3DXVECTOR3 startPosition, D3DXVECTOR3 endPosition);

	//現在のアニメーションのフレームを返す
	int GetAnimationFrame();

	//カードを引くアニメーションが終わったか
	bool IsEndPullAnimation();

	//カードの集合
	void GatherMoveCards(D3DXVECTOR3 endPosition, int positionNum);

	//カードの解散
	void RemoveCards(D3DXVECTOR3 startPosition, int positionNum);

	//カードを捨てる（捨て札の位置に移動）
	void Discard(D3DXVECTOR3 endPosition);

	//山札に移動
	void moveToDeck();

	//アクセス関数
	void SetCardData(CardData* cardData) { data_ = *cardData; }
	CardData* GetCardData() { return &data_; }
	void SetDisplay(bool display) { display_ = display; }
	void SetSelected(bool selected) { selected_ = selected; }
	bool GetSelected() { return selected_; }
	void SetState(int state) { state_ = state; }
	int GetState() { return state_; }
	int GetHandle() { return hModel_; }

	//カードのもとの位置の記録
	void SetRemovePosition(D3DXVECTOR3 Position) { removePosition_ = Position; }
	D3DXVECTOR3 GetRemovePosition() { return removePosition_; }
};