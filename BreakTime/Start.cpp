#include "Start.h"
#include "Engine/Image.h"

//コンストラクタ
Start::Start(IGameObject * parent)
	:IGameObject(parent, "Start"),
	hPict_(-1), stride_(0.01f), clear_(1)
{
}

//デストラクタ
Start::~Start()
{
}

//初期化
void Start::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/start.png");
	assert(hPict_ >= 0);

	position_ = D3DXVECTOR3(MOVE_WIDTH, MOVE_HEIGHT, 0);
}

//更新
void Start::Update()
{

	//if (Input::IsKeyDown(DIK_SPACE))
	//{
	//	clear_ = 1;
	//}
	//switch (state_)
	//{
	//case Start::STATE_DARK:
	//	clear_ += stride_;
	//	if (clear_ >= 1.0f)
	//	{
	//		state_ = STATE_BRIGTH;
	//		clear_ = 1.0f;
	//	}
	//	break;
	//case Start::STATE_BRIGTH:
	//	clear_ -= stride_;
	//	if (clear_ <= 0)
	//	{
	//		state_ = STATE_DARK;
	//		clear_ = 0;
	//	}
	//	break;
	//}

	//指定した画像の透明度をセット
	Image::SetColor(hPict_, clear_);
}

//描画
void Start::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Start::Release()
{
}


