#pragma once
#include "Engine/IGameObject.h"

//ヘルプボードを管理するクラス
class Help : public IGameObject
{
private:
	int hImage_;		//モデル番号

	const float moveDis_ = 5.0f;
	float move_;

	bool flag_;

public:
	//コンストラクタ
	Help(IGameObject* parent);

	//デストラクタ
	~Help();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ヘルプ移動(登場)
	void MoveDis(bool flag = false);
};