#pragma once
#include "Engine/IGameObject.h"

//タイトル画像を管理するクラス
class TitleImage : public IGameObject
{
private:
	int hPict_;

	float clear_;		// α値更新
	float stride_;		// ↑の変化式

public:
	//コンストラクタ
	TitleImage(IGameObject* parent);

	//デストラクタ
	~TitleImage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//アクセス関数
	float Getclear() { return clear_; }
};