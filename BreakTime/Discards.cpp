#include "Discards.h"
#include "Card.h"

//コンストラクタ
Discards::Discards(IGameObject * parent)
	:IGameObject(parent, "Discards"), isReset_(false)
{
}

//デストラクタ
Discards::~Discards()
{
}

//初期化
void Discards::Initialize()
{
	//位置
	SetPosition(D3DXVECTOR3(-3, 0, -1));
}

//更新
void Discards::Update()
{
	//リセット時
	if (isReset_)
	{
		bool doReset = false;
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			if (((Card*)(*it))->GetState() == CARD_STATE_WAIT)
			{
				doReset = true;
			}
			else
			{
				doReset = false;
				break;
			}
		}

		if (doReset)
		{
			for (auto it = childList_.begin(); it != childList_.end(); it++)
			{
				((Card*)(*it))->SetState(CARD_STATE_RESET);
			}
		}
	}
}

//描画
void Discards::Draw()
{
}

//開放
void Discards::Release()
{
}

//カード追加
void Discards::AddCard(CardData * cardData)
{
	////カード生成
	//Card* pCard = CreateGameObject<Card>(this);
	////カード情報を設定
	//pCard->SetCardData(cardData);
}

//捨て札をリセット
void Discards::Reset()
{
	//カード全てを山札の位置に移動
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		((Card*)(*it))->moveToDeck();
	}

	isReset_ = true;

	//カードをすべて削除
	/*for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->KillMe();
	}*/
}


