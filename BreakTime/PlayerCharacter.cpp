#include "PlayerCharacter.h"
#include "Engine/Image.h"
#include "Card.h"
#include "Hand.h"
#include "PlayScene.h"
#include "Deck.h"
#include "BetBoard.h"
#include "FoldOrCall.h"

#define ADD_POINT		10			//ポイント増減の単位
#define BET_HEIGHT		370			//賭けポイント表示の高さ
#define BET_WIDTH		790			//賭けポイント表示の幅
#define POINT_HEIGHT	450			//所持ポイント表示の高さ
#define POINT_WIDTH		700			//所持ポイント表示の幅


PlayerCharacter::PlayerCharacter(IGameObject * parent) : Player(parent), pFoldOrCall_(nullptr)
{
}


PlayerCharacter::~PlayerCharacter()
{
}

void PlayerCharacter::InitHandCardsPos()
{
	float x = -1.9f;
	for (int i = 0; i < HAND_MAX; i++)
	{
		pHand_->GetChild(i)->SetRotate(D3DXVECTOR3(0, 0, 0));
		pHand_->GetChild(i)->SetPosition(D3DXVECTOR3(x, 0, -5));
		x += 1.2f;
	}
}

void PlayerCharacter::InitBetBoardPos()
{
	pBetBoard_->SetBetPosition(D3DXVECTOR3(BET_WIDTH, BET_HEIGHT, 0.5));
	pBetBoard_->SetPointPosition(D3DXVECTOR3(POINT_WIDTH, POINT_HEIGHT, 0.5));

	//
	pFoldOrCall_ = CreateGameObject<FoldOrCall>(this);
}

void PlayerCharacter::Draw()
{
	/*D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 50, 390, 0);

	Image::SetMatrix(hPict_[wins_], worldMatrix_ * m);
	Image::Draw(hPict_[wins_]);*/
}

void PlayerCharacter::PullCard(int addPosition)
{
	CardData* pull = ((PlayScene*)pParent_)->GetDeck()->PullCard();
	pHand_->PullCard(pull, addPosition, CHARACTER_PC);
}

bool PlayerCharacter::betPoint()
{
	//[↓]キー
	if (Input::IsKeyDown(DIK_DOWNARROW))
	{
		//賭けポイント増加
		pBetBoard_->AddBetPoint(-ADD_POINT);
	}

	//[↑]キー
	if (Input::IsKeyDown(DIK_UPARROW))
	{
		//賭けポイント減少
		pBetBoard_->AddBetPoint(ADD_POINT);
	}

	//[Enter]キー
	if (Input::IsKeyDown(DIK_RETURN) && (pBetBoard_->GetBetPoint() > 0))
	{
		//賭けポイント決定
		return true;
	}
}

bool PlayerCharacter::SelectFoldOrCall()
{
	//選択用UI表示
	pFoldOrCall_->Display(true);

	//[→]キー
	if (Input::IsKeyDown(DIK_RIGHTARROW))
	{
		//カーソルを「降りる」に
		pFoldOrCall_->SelectCursor(SELECT_FOLD);
		
	}

	//[←]キー
	if (Input::IsKeyDown(DIK_LEFTARROW))
	{
		//カーソルを「勝負」に
		pFoldOrCall_->SelectCursor(SELECT_CALL);
	}

	//[Enter]キー
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//現在選択しているものを保存
		switch (pFoldOrCall_->GetSelect())
		{
		case SELECT_CALL:
			isCalled_ = true;
			break;

		case SELECT_FOLD:
			isCalled_ = false;
			break;

		default:
			return false;
		}

		//選択用UI非表示
		pFoldOrCall_->Display(false);

		//賭けポイント決定
		return true;
	}

	//まだ選択中
	return false;
}
