#pragma once
#include "Engine/IGameObject.h"

//数字
enum NUMBER {
	NUMBER_0,
	NUMBER_1,
	NUMBER_2,
	NUMBER_3,
	NUMBER_4,
	NUMBER_5,
	NUMBER_6,
	NUMBER_7,
	NUMBER_8,
	NUMBER_9,
	NUMBER_MAX
};

//矢印
enum ARROW {
	ARROW_UP,
	ARROW_DOWN,
	ARROW_MAX
};

//ポイントボードを管理するクラス
class BetBoard : public IGameObject
{
	int hPictBet_;							//賭けポイント表示画像番号
	int hPictPoint_;						//所持ポイント表示画像番号
	int hPictNumber_[NUMBER_MAX];			//数字画像番号
	int hPictArrow_[ARROW_MAX];				//ポイント選択時の矢印画像番号	
	std::string fileName_[NUMBER_MAX];		//テキストのファイル名
	D3DXVECTOR3 betPosition_;				//賭けポイントの表示位置
	D3DXVECTOR3 pointPosition_;				//所持ポイントの表示位置
	D3DXVECTOR3 numberPosition_;			//数字表示位置
	int point_;								//所持ポイント
	int betPoint_;							//賭けポイント
	bool isDisplayArrow_;					//表示するかどうかのフラグ
	LPD3DXEFFECT pEffect_;					//エフェクト
	int arrowCount_;						//矢印表示用のカウント

public:
	//コンストラクタ
	BetBoard(IGameObject* parent);

	//デストラクタ
	~BetBoard();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//賭けポイントを追加
	//引数：point 追加するポイント
	void AddBetPoint(int point);

	//所持ポイントと賭けポイントを表示
	void DrawPoint();

	//任意の位置に数字を表示
	//引数：position 表示位置,	number 数字
	void DrawNumber(D3DXVECTOR3 position, int number);

	//ポイント選択用の矢印描画
	void DrawArrow();

	//ポイント選択用の矢印表示切替
	//引数：isDisplay 表示するかどうか
	void DisplayArrow(bool isDisplay);

	//ポイントを加算
	//引数：point 加えるポイント
	void AddPoint(int point) { point_ += point; }

	//ポイントをリセット
	void ResetPoint();

	//アクセス関数
	void SetPointPosition(D3DXVECTOR3 position) { pointPosition_ = position; }
	void SetBetPosition(D3DXVECTOR3 position) { betPosition_ = position; }
	int GetBetPoint() { return betPoint_; }
	int GetPoint() { return point_; }
	void SetBetPoint(int point) { betPoint_ = point; }
};