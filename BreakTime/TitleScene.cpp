#include "TitleScene.h"
#include "Engine/Image.h"
#include "BackGround.h"
#include "TitleImage.h"
#include "Start.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), changeTime_(60), pBackGround_(nullptr), isStart_(false)
{
}

//初期化
void TitleScene::Initialize()
{
	//背景クラスを子供に
	pBackGround_ = CreateGameObject<BackGround>(this);

	//スタート時間のセット
	time(&start_);

}

//更新
void TitleScene::Update()
{
	now_ = time(&now_);
	double difference = difftime(now_, start_);
	if (difference > changeTime_)
	{
		//スプラッシュ画面へ
		SceneManager::ChangeScene(SCENE_ID_SPLASH);
	}

	if (Input::IsKeyDown(DIK_SPACE) && pBackGround_->Getclear() >= 1.0f)
	{
		start_ = time(&start_);
		//メニュー画面へ
		SceneManager::ChangeScene(SCENE_ID_MENU);
	}

	if (pBackGround_->GetLoadFlag_() == true && isStart_ == false)
	{
		pStart_ = CreateGameObject<Start>(this);
		isStart_ = true;
	}
}

//描画
void TitleScene::Draw()
{

}

//開放
void TitleScene::Release()
{
}