#include <time.h>
#include <stdlib.h>
#include "Deck.h"
#include "PlayScene.h"
#include "Discards.h"
#include "Engine/Model.h"
#include "Card.h"

//コンストラクタ
Deck::Deck(IGameObject * parent)
	:IGameObject(parent, "Deck"), hModel_(-1), aryPullRange_(DECK_MAX - 1), cardDataAry_(nullptr)
{
}

//デストラクタ
Deck::~Deck()
{
}

//初期化
void Deck::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/deck.fbx");
	assert(hModel_ >= 0);

	//山札の一番上にカードを生成
	Card* pCard = CreateGameObject<Card>(this);
	pCard->SetDisplay(true);
	int cardModelHandle = pCard->GetHandle();
	Model::SetAnimFrame(cardModelHandle, 1, 1, 0);

	//山札の初期化
	cardDataAry_ = new CardData[DECK_MAX];
	int i = 0;
	for (int initSuit = SUIT_SPADE; initSuit < SUIT_MAX; initSuit++)
	{
		for (int num = 1; num <= CARD_NUMMAX; num++, i++)
		{
			cardDataAry_[i].suit = (SUIT)initSuit;
			cardDataAry_[i].number = num;
		}
	}
	//乱数種を時間から取得
	srand((unsigned)time(NULL));

	//位置
	SetPosition(D3DXVECTOR3(3, 0, -1));
}

//更新
void Deck::Update()
{
	//山札の枚数が半分以下
	if (aryPullRange_ < DECK_MAX / 2)
	{
		//捨て札をリセット
		((PlayScene*)pParent_)->GetDiscards()->Reset();

		//山札をリセット
		aryPullRange_ = DECK_MAX - 1;
	}
}

//描画
void Deck::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Deck::Release()
{
	/* 開放してはいるものの怪しいかも */
	delete(cardDataAry_);
}

CardData* Deck::PullCard()
{
	int selectIndex = aryPullRange_;
	int random = rand() % aryPullRange_;
	//選ばれたカードを最後尾へ
	CardData wsp = cardDataAry_[aryPullRange_];
	cardDataAry_[aryPullRange_] = cardDataAry_[random];
	cardDataAry_[random] = wsp;
	//ランダムの範囲を狭める
	if (aryPullRange_ > 0)
	{
		aryPullRange_--;
	}
	return &cardDataAry_[selectIndex];
}