#include "Help.h"
#include "Engine/Image.h"

const float SCALE_X = 1.5f;
const float SCALE_Y = 2;
const float POS_X = 270;
const float POS_Y = -400;

//コンストラクタ
Help::Help(IGameObject * parent)
	:IGameObject(parent, "Help"), hImage_(-1), move_(0), flag_(false)
{
}

//デストラクタ
Help::~Help()
{
}

//初期化
void Help::Initialize()
{
	//モデルデータのロード
	hImage_ = Image::Load("data/rule_2.png");
	assert(hImage_ >= 0);

	Image::SetColor(hImage_, 1.0);

	scale_ = D3DXVECTOR3(scale_.x / SCALE_X, scale_.y / SCALE_Y, scale_.z);
	SetPositionX(POS_X);
	SetPositionY(POS_Y);
}

//更新
void Help::Update()
{
	
	if (position_.y >= 0  && flag_ == false)
	{
		move_ = 0;

	}
	else if(position_.y > -400 && flag_ == true)
	{
		move_ = -5.0f;
	}

	position_.y += move_;
}

//描画
void Help::Draw()
{
	Image::SetMatrix(hImage_, worldMatrix_);
	Image::Draw(hImage_);
}

//開放
void Help::Release()
{
}

void Help::MoveDis(bool flag)
{
	move_ = moveDis_;
	flag_ = flag;
}
